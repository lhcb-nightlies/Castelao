#ifndef TYPESTAT_H
#define TYPESTAT_H 1
 
// Include files
 
// from Gaudi
#include "TrackCheckerBase.h"

#include "Event/Track.h"

namespace LHCb{
  class MCParticle;
}

/** @class TypeStat TypeStat.h
 * 
 *  @author Paul Seyfert
 *  @date   21-04-2012
 */                 
                                                           
class TypeStat : public TrackCheckerBase {
                                                                             
 public:
                                                                             
  /** Standard construtor */
  TypeStat( const std::string& name, ISvcLocator* pSvcLocator );
                                                                             
  /** Destructor */
  virtual ~TypeStat();

  /** Algorithm initialize */
  virtual StatusCode initialize();

  /** Algorithm execute */
  virtual StatusCode execute();

  /** Algorithm finalize */
  virtual StatusCode finalize();
  
 private:

  std::vector< std::vector<int> > m_linkedIds;
  
  std::vector<int> clonepattern(LHCb::Tracks* tracks, LHCb::Tracks::const_iterator iterout);

  bool m_requireLong;

  void ghostInfo(const LHCb::Tracks* tracks);

  void effInfo();

  void plots(const std::string& type, 
             const LHCb::Track* track ) const;

  void plots(const std::string& type, 
             const LHCb::MCParticle* part) const;

  double weightedMeasurementSum(const LHCb::Track* aTrack) const;

};

#endif // TYPESTAT_H
