# How to train the ghost probability?

## create a training ntuple

### intro

This howto is written for the case of just updating the weights of the Run2
GhostProbability. Anything more fancy (changing input variables) will require
more work. Please still read the entire howto. The deviations should be obvious.

If you want to revisit the choice of input variables (adding new ones because
they add discrimination) you need to follow the DaVinci path below. Otherwise,
Brunel is more fool proof (no need for track refitting).

### option a) Brunel

Set up the appropriate Brunel version. Check out Tr/TrackTools and
Tr/TrackingScripts. Copy C++ content of
Tr/TrackingScripts/GhostProbability/ntuple_production/Run2Brunel
to Tr/TrackTools/src.
Edit Tr/TrackTools/cmt/requirements and add
```
use   MCInterfaces      v*   Kernel
use   LinkerEvent       v*   Event
```
FIXME: for cmake builds, something CMakeLists.txt needs to be changed accordingly.

build and run the a Brunel job with the options
Tr/TrackingScripts/GhostProbability/ntuple_production/brunelopts.py

#### remarks

The brunel ntuple tool is designed for robustness. It effectively runs a person
in the middle attack against the ghost probability - the input variables of the
ghost probability get grabbed and written to an ntuple. The tool you copied
from the TrackingScripts only forwards information back and forth. This means,
that with appropriate configuration, you can also monitor any running
ghostprobability.

### option b) DaVinci

There are two tuple tools in Tr/TrackingScripts/ntuple_production/Run2DaVinci,
which you can add to Phys/DecayTreeTupleReco/src (one of them is a small
modification to one which is already in there).  The options in that directory
should run out of the box. You might still want to revisit the settings (e.g.
DataType, database tags, fit configuration)

## running TMVA

(editing TMVALong_varsets should be straight forward if you used TMVA in the past)

The program TMVALong_varsets in
Tr/TrackingScripts/GhostProbability/TMVA_training is the program you're looking
for. It's arguments are:

a) an integer to pick the right ntuple to train from (I had four, with
different inst. luminosities and different bunch spacing. Search for `TChain`
in the code and read.)

b) the input variables binary encoded. If you do not provide the right number
of arguments, a help message gets printed:
    std::cout << "USAGE" << std::endl;
    std::cout << argv[0] << " <inputntupleID> <variables> <tracktype> "<<std::endl;
    std::cout << "" << std::endl;
    std::cout << "variables = a*2^0 + b*2^1 + c*2^2 +d*2^3 + e*2^4 " << std::endl;
    std::cout << "   where a = use observed hit pattern" << std::endl;
    std::cout << "         b = use expected hit pattern" << std::endl;
    std::cout << "         c = use detector occupancies" << std::endl;
    std::cout << "         d = use outliers from fit   " << std::endl;
    std::cout << "         e = use kinematicsit pattern" << std::endl;

in early run2 we were using 29.

c) the track type (1,3,4,5,6)

in short, just run:
```
make TMVALong_varsets
./TMVALong_varsets 0 29 1 > training_1.log &
./TMVALong_varsets 0 29 3 > training_3.log &
./TMVALong_varsets 0 29 4 > training_4.log &
./TMVALong_varsets 0 29 5 > training_5.log &
./TMVALong_varsets 0 29 6 > training_6.log
```

### you want to do fancy trainings

scroll through the code to see how to train category classifiers (i.e.
different networks for match tracks and forward tracks. not sure if these work
with the brunel ntuples. they were designed for the davinci ntuples)

The call of the methods is packed into:
```
 bool alsoFancyStuff = false;
 if (alsoFancyStuff) {
```

## copying the new weights into the TMVA files

(this will be more complicated if you add input variables)

for performance reasons, I slightly edited the .class.C files which come out of
TMVA. I recommend to use a graphical diff program (e.g. meld) to copy the hard
coded numeric values from the newly trained .class.C files in your weights/
directory into the files in Tr/TrackingScripts/src/TMVA.

## updating the flattening tables

(should not depend at all on the choice of input variables)

Create flattening tables (these things which transform the network response
such that ghost retention \approx cut value; these things are nice because
existing cut values in the stripping won't be completely off all of a sudden):

figure out where the .root files from TMVA got stored. The filenames contain
the arguments from the call of TMVALong_varset, up to a modification that the
training ntuple name is human readable (instead of an integer) and the variable
set in hexadecimal (instead of decimal).

```
root -l
.L MakeNewFlattening.C+O
MakeFlatteningFunction("/path/to/next_TMVA_1_25nsLL_1d.root","TrainTree","MLP",false,200,"FlattenVelo.C","VeloTable")
MakeFlatteningFunction("/path/to/next_TMVA_3_25nsLL_1d.root","TrainTree","MLP",false,200,"FlattenLong.C","LongTable")
MakeFlatteningFunction("/path/to/next_TMVA_4_25nsLL_1d.root","TrainTree","MLP",false,200,"FlattenUpstream.C","UpstreamTable")
MakeFlatteningFunction("/path/to/next_TMVA_5_25nsLL_1d.root","TrainTree","MLP",false,200,"FlattenDownstream.C","DownstreamTable")
MakeFlatteningFunction("/path/to/next_TMVA_6_25nsLL_1d.root","TrainTree","MLP",false,200,"FlattenTtrack.C","TtrackTable")
.q
```

"MLP" is the name of the classifier in the TMVA training.
200 is the granularity of the table. 200 means that the ghostprobability
    response is rendered to an accuracy of 1/200 = 0.005.

The resulting .C files should be ready to go to Tr/TrackingScripts/src/TMVA.

use a graphical diff program to ensure yourself of what you're doing.

## debugging and stuff

the TMVA_add.C program should help you adding your freshly trained TMVA network
to the davinci or brunel ntuples. (if you want to have the response of one
training ntuple on a different ntuple - comparing 25ns to 50ns simulation).
