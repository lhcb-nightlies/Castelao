from Configurables import LHCbApp

LHCbApp().DDDBtag   = "dddb-20140729"
LHCbApp().CondDBtag = "sim-20140730-vc-md100"
#dddb-20140729 Condition DB: sim-20140730-vc-md100

##############################################################################
# File for running Brunel on 2012 data
##############################################################################

from Configurables import Brunel
Brunel().DataType  = "2015"

##############################################################################
##############################################################################
# File for running Brunel on MC data and saving all MC Truth
# DataType should be set separately
#
# Syntax is:
# gaudirun.py Brunel/MC-WithTruth.py Conditions/<someTag>.py <someDataFiles>.py
##############################################################################

from Configurables import Brunel

Brunel().InputType = "DIGI"
Brunel().WithMC    = True
Brunel().SplitRawEventOutput = 2.0

##############################################################################
# These options will set the flag necessary to have simulation dst with Boole linkers added

from Configurables import Brunel

Brunel().OutputType = 'LDST'

from Configurables import RecMoniConf
RecMoniConf().MoniSequence = []

from Configurables import L0Conf
L0Conf().EnsureKnownTCK=False
# Change algorithm for ROOT compression of output files
# This setting is recommended for writing temporary intermediate files, for
# which speed of writing is more important than file size on disk

from Configurables import RootCnvSvc
RootCnvSvc().GlobalCompression = "ZLIB:1"

from Gaudi.Configuration import appendPostConfigAction

def ntupletool():
    from Configurables import TrackAddNNGhostId,GaudiSequencer
    TrackAddNNGhostId("test").GhostIdTool = "Run2GhostIdNT"
    GaudiSequencer("MCLinksTrSeq").Members += [TrackAddNNGhostId("test")]
appendPostConfigAction(ntupletool)
from Configurables import NTupleSvc
NTupleSvc().Output   = ["FILE1 DATAFILE='test.root' TYP='ROOT' OPT='NEW'"]

