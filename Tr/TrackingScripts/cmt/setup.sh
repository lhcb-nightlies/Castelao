# echo "Setting TrackingScripts v1r0 in /home/hep/decianm/cmtuser/DaVinci_v33r2/Tr"

if test "${CMTROOT}" = ""; then
  CMTROOT=/cvmfs/lhcb.cern.ch/lib/contrib/CMT/v1r20p20090520; export CMTROOT
fi
. ${CMTROOT}/mgr/setup.sh

tempfile=`${CMTROOT}/mgr/cmt -quiet build temporary_name`
if test ! $? = 0 ; then tempfile=/tmp/cmt.$$; fi
${CMTROOT}/mgr/cmt setup -sh -pack=TrackingScripts -version=v1r0 -path=/home/hep/decianm/cmtuser/DaVinci_v33r2/Tr  -no_cleanup $* >${tempfile}; . ${tempfile}
/bin/rm -f ${tempfile}

