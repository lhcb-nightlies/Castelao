The TupleToolTrackEff is a tuple tool to check if a MuonTT track (from the "Long Method") is matched to a long track. In normal operation mode, it just returns this basic information, if isVerbose == True, more variables are returned that might help studying systematic effects or tuning the cuts.

An example, how the TupleToolTrackEff can be used is given in TrackEffJpsiMC.

-- Michel De Cian, michel.de.cian@cern.ch, 21.1.2013 --
