#ifndef Run2GhostIdNT_H
#define Run2GhostIdNT_H 1

// Include files
#include "Linker/LinkerTool.h"

#include "TrackInterfaces/IHitExpectation.h"
//#include "TrackInterfaces/IVPExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"
#include "OTDAQ/IOTRawBankDecoder.h"
#include "TrackInterfaces/IGhostProbability.h"
#include "GaudiAlg/GaudiTupleTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "MCInterfaces/ITrackGhostClassification.h"



class IClassifierReader;


/** @class Run2GhostIdNT Run2GhostIdNT.h
 *
 *  @author Paul Seyfert
 *  @date   06-02-2015
 */
class Run2GhostIdNT : public GaudiTupleTool,
                       virtual public IGhostProbability {


public:
  /// Standard constructor
  Run2GhostIdNT( const std::string& type,
                  const std::string& name,
                  const IInterface* parent);



  virtual ~Run2GhostIdNT(){}; ///< Destructor

  virtual StatusCode finalize(); ///< Algorithm initialization
  virtual StatusCode initialize(); ///< Algorithm initialization
  virtual StatusCode execute(LHCb::Track& aTrack) const; 
  virtual StatusCode beginEvent() {return m_ghostTool->beginEvent();};
  virtual std::vector<std::string> variableNames(LHCb::Track::Types type) const {return m_ghostTool->variableNames(type);};
  virtual std::vector<float> netInputs(LHCb::Track& aTrack) const {return m_ghostTool->netInputs(aTrack);};


protected:

private:
  ITrackGhostClassification* m_classification;
  IGhostProbability* m_ghostTool;

};

#endif // Run2GhostIdNT_H
