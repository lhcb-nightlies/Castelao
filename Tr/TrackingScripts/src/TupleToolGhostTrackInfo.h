#ifndef TUPLETOOLGhostTrackInfo_H
#define TUPLETOOLGhostTrackInfo_H 1

// Include files
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"
#include "MCInterfaces/ITrackGhostClassification.h"



/** @class TupleToolGhostTrackInfo
 *
 *  @author Paul Seyfert
 *  @date   2016-09-12
 */
class TupleToolGhostTrackInfo : public TupleToolBase,
                           virtual public IParticleTupleTool {
public:
  /// Standard constructor
  TupleToolGhostTrackInfo( const std::string& type,
		    const std::string& name,
		    const IInterface* parent);

  virtual ~TupleToolGhostTrackInfo(){}; ///< Destructor

  virtual StatusCode initialize(); ///< Algorithm initialization

  virtual StatusCode fill( const LHCb::Particle*
			   , const LHCb::Particle*
			   , const std::string&
			   , Tuples::Tuple& );
  
private:
  ITrackGhostClassification*    m_ghostClassification;

};

#endif // TUPLETOOLGhostTrackInfo_H
