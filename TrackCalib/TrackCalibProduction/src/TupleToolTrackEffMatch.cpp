// Include files 
#include "gsl/gsl_sys.h"
#include <boost/foreach.hpp>

// from Gaudi
#include "GaudiKernel/ToolFactory.h" 
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/Vector3DTypes.h"

// local
#include "TupleToolTrackEffMatch.h"

#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolTrackEffMatch
//
// 2017-07-26 : Michael Kolpin
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_TOOL_FACTORY( TupleToolTrackEffMatch )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolTrackEffMatch::TupleToolTrackEffMatch( const std::string& type,
                                        const std::string& name,
                                        const IInterface* parent )
  : TupleToolBase ( type, name , parent )
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("MatchedLocation1", m_matchedLocation1 = "/Event/Turbo/Hlt2TrackEffDiMuonMuonTTPlusMatchedTurboCalib/Particles");
  declareProperty("MatchedLocation2", m_matchedLocation2 = "/Event/Turbo/Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedTurboCalib/Particles");
  declareProperty("MC", m_MC = true);
}
//=============================================================================
// Destructor
//=============================================================================
TupleToolTrackEffMatch::~TupleToolTrackEffMatch() {} 

//=============================================================================
// initialize
//=============================================================================
StatusCode TupleToolTrackEffMatch::initialize(){
  if( ! TupleToolBase::initialize() ) return StatusCode::FAILURE;

  std::string p2mcAssocType("DaVinciSmartAssociator");
  m_p2mcAssoc = tool<IParticle2MCAssociator>(p2mcAssocType,this);

  
  return StatusCode::SUCCESS ;
}
//=============================================================================
// Fill
//=============================================================================
StatusCode TupleToolTrackEffMatch::fill( const LHCb::Particle* top
                                     , const LHCb::Particle* part
                                     , const std::string& head
                                     , Tuples::Tuple& tuple )
{
  //warning() << "TupleToolTrackEffMatch starting!" << endmsg;
  const std::string prefix=fullName(head);
  warning() << "Starting fill" << endmsg;
  
  // -- Skip the mother part.
  if( part == top ){
    if ( msgLevel(MSG::VERBOSE) ) verbose() << "Mother resonance does not have track: skipping" << endmsg;
    return StatusCode::SUCCESS ;
  }
  
  // -- Navigate back from the probe particle to the track.
  const LHCb::ProtoParticle* proto = part->proto();
  if(!proto){
    if ( msgLevel(MSG::WARNING) ) warning() << "Could not find protoparticle corresponding to particle" << endmsg;
    return StatusCode::SUCCESS ;
  }
   
  const LHCb::Track* probeTrack = proto->track();
  if(!probeTrack){
    if ( msgLevel(MSG::WARNING) ) warning() <<  "Could not find track corresponding to protoparticle" << endmsg;
    return StatusCode::SUCCESS ;
  }

  // ---------------------------------------------------------------
  
  warning() << "Opening match locations" << endmsg;
  
  // -- Get all long tracks in the event in the proper Tesla container
  if( !exist<LHCb::Particles>(m_matchedLocation1) && !exist<LHCb::Particles>(m_matchedLocation2)){
    if ( msgLevel(MSG::WARNING) ) warning() << "Containers " << m_matchedLocation1 << " and " << m_matchedLocation2 << " do not exist" << endmsg;
    tuple->column(head+"_Assoc", false);
    tuple->column(head+"_Overlap_TTFraction", -1.);
    tuple->column(head+"_Overlap_TFraction", -1.);
    tuple->column(head+"_Overlap_ITFraction", -1.);
    tuple->column(head+"_Overlap_OTFraction", -1.);
    tuple->column(head+"_Overlap_VeloFraction", -1.);
    tuple->column(head+"_Overlap_MuonFraction", -1.);
    tuple->column( head+"_Matched_StateCTB_tx",-1.e6 );
    tuple->column( head+"_Matched_StateCTB_ty",-1.e6 );
    tuple->column( head+"_Matched_StateCTB_x", -1.e6);
    tuple->column( head+"_Matched_StateCTB_y", -1.e6);
    tuple->column( head+"_Matched_StateCTB_z", -1.e6);
    tuple->column(head+"_Matched_Px", -1.e6);
    tuple->column(head+"_Matched_Py", -1.e6);
    tuple->column(head+"_Matched_Pz", -1.e6);
    tuple->column(head+"_Matched_P",  -1.e6);
    tuple->column(head+"_Matched_dP", -1.e6);
    tuple->column(head+"_Matched_probChi2", -1.e6 );
    tuple->column(head+"_Matched_Chi2NDoF", -1.e6);
    tuple->column(head+"_Matched_VeloChi2", -1.e6);
    tuple->column(head+"_Matched_VeloNDoF", -1.e6);
    tuple->column(head+"_Matched_GhostProb", -1.e6);
    tuple->column(head+"_DeltaR", -1.);
    tuple->column(head+"_DeltaP", -1.);
    tuple->column(head+"_Matched_SameMCPart", 0);
    tuple->column(head+"_MCPart", false);
    tuple->column(head+"_Matched_MCPart", false);
    return StatusCode::SUCCESS;
  }
  warning() << "Matched container found, reading in tracks!" << endmsg;
  
  const LHCb::Track* assocTrack = NULL;
  const LHCb::MuonPID* assocMuPID = NULL;
  bool assoc = false;
  const LHCb::Particle::Range parts1 = getIfExists<LHCb::Particle::Range>(m_matchedLocation1);
  const LHCb::Particle::Range parts2 = getIfExists<LHCb::Particle::Range>(m_matchedLocation2);
  const LHCb::Particle *longPart = NULL;
  const LHCb::ProtoParticle *longProto = NULL;
  std::vector<LHCb::Particle> longParts;
  std::vector<LHCb::Track> longTracks;
  std::vector<LHCb::MuonPID> longMuPIDs;

  // -- Loop over all particles
  if (exist<LHCb::Particles>(m_matchedLocation1)){ 
      BOOST_FOREACH( const LHCb::Particle* tmpJpsi, parts1) {
    //only successfully matched tracks are saved, where pion ID is assigned to the particle
    //multiple tracks with sufficient overlap can be found, fill into vector and check for overlap of each with probe
      const LHCb::Particle::ConstVector jpsi_daughters = tmpJpsi->daughtersVector();
      BOOST_FOREACH( const LHCb::Particle* tmpPart, jpsi_daughters) {
          if ((fabs(tmpPart->particleID().pid()) == 211 ) ){
              longPart = tmpPart;
              longParts.push_back(*longPart);
              longProto = tmpPart->proto();
              assocTrack = longProto->track();
              if (longProto->muonPID()) {
                  assocMuPID = longProto->muonPID();
                  longMuPIDs.push_back(*assocMuPID);
              }
              longTracks.push_back(*assocTrack);
              assoc = true;

          }
      }
    }
  }
  if (exist<LHCb::Particles>(m_matchedLocation2)) {
      BOOST_FOREACH( const LHCb::Particle* tmpJpsi, parts2) {
    //only successfully matched tracks are saved, where pion ID is assigned to the particle
    //multiple tracks with sufficient overlap can be found, fill into vector and check for overlap of each with probe
      const LHCb::Particle::ConstVector jpsi_daughters = tmpJpsi->daughtersVector();
      BOOST_FOREACH( const LHCb::Particle* tmpPart, jpsi_daughters) {
          if ((fabs(tmpPart->particleID().pid()) == 211 ) ){
              longPart = tmpPart;
              longParts.push_back(*longPart);
              longProto = tmpPart->proto();
              assocTrack = longProto->track();
              if (longProto->muonPID()) {
                  assocMuPID = longProto->muonPID();
                  longMuPIDs.push_back(*assocMuPID);
              }
              longTracks.push_back(*assocTrack);
              assoc = true;

          }
      }
    }
  }

  
  //loop over all formerly matched track and keep the one with highest overlap; TODO: sum of subdetector overlaps, better choice?
  double maxOverlap = 0;

  bool sameCharge = ((longPart->particleID().pid()/(fabs(longPart->particleID().pid())))==-(part->particleID().pid()/(fabs(part->particleID().pid())))); 

  double TTFracMatch = -1.;
  double TFracMatch = -1.;
  double ITFracMatch = -1.;
  double OTFracMatch = -1.;
  double VeloFracMatch = -1.;
  double MuonFracMatch = -1.;

  //only do this for the probe track, i.e. same charge as matched track
  if (sameCharge){
      for ( std::vector<LHCb::Track>::iterator it = longTracks.begin(); it != longTracks.end(); ++it){
          double TTFracMatchTmp = -1.;
          double TFracMatchTmp = -1.;
          double ITFracMatchTmp = -1.;
          double OTFracMatchTmp = -1.;
          double VeloFracMatchTmp = -1.;
          double MuonFracMatchTmp = -1.;
          
          LHCb::Track *loopTrack = &(*it);
          //LHCb::MuonPID *loopMuPID = &(longMuPIDs.at(0));
          LHCb::MuonPID *loopMuPID = NULL;
          if ( (longTracks.size()) == (longMuPIDs.size()) ){ 
              LHCb::MuonPID *tmpMuPID = &(longMuPIDs.at(it-longTracks.begin()));
              loopMuPID = tmpMuPID;
          }

          commonLHCbIDs(loopTrack, probeTrack, loopMuPID, MuonFracMatchTmp, TTFracMatchTmp, TFracMatchTmp, ITFracMatchTmp, OTFracMatchTmp, VeloFracMatchTmp);
          if (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp > maxOverlap){
              maxOverlap = (MuonFracMatchTmp+TTFracMatchTmp+TFracMatchTmp+VeloFracMatchTmp);
              assocTrack = loopTrack;
              longPart = &longParts.at(it-longTracks.begin());
              TTFracMatch = TTFracMatchTmp;
              TFracMatch = TFracMatchTmp;
              ITFracMatch = ITFracMatchTmp;
              OTFracMatch = OTFracMatchTmp;
              VeloFracMatch = VeloFracMatchTmp;
              MuonFracMatch = MuonFracMatchTmp;
          }
      }
  }
 
  tuple->column(head+"_Assoc", assoc);
  tuple->column(head+"_Overlap_TTFraction", TTFracMatch);
  tuple->column(head+"_Overlap_TFraction", TFracMatch);
  tuple->column(head+"_Overlap_ITFraction", ITFracMatch);
  tuple->column(head+"_Overlap_OTFraction", OTFracMatch);
  tuple->column(head+"_Overlap_VeloFraction", VeloFracMatch);
  tuple->column(head+"_Overlap_MuonFraction", MuonFracMatch);


  const LHCb::State *stateBeam = NULL;
  if ( assocTrack && assocTrack->hasStateAt(LHCb::State::ClosestToBeam) ){
      stateBeam = assocTrack->stateAt(LHCb::State::ClosestToBeam);
      tuple->column( head+"_Matched_StateCTB_tx",stateBeam->tx() );
      tuple->column( head+"_Matched_StateCTB_ty",stateBeam->ty() );
      tuple->column( head+"_Matched_StateCTB_x",stateBeam->x() );
      tuple->column( head+"_Matched_StateCTB_y",stateBeam->y() );
      tuple->column( head+"_Matched_StateCTB_z",stateBeam->z() );
  }
  else{
      tuple->column( head+"_Matched_StateCTB_tx",-1.e6 );
      tuple->column( head+"_Matched_StateCTB_ty",-1.e6 );
      tuple->column( head+"_Matched_StateCTB_x", -1.e6);
      tuple->column( head+"_Matched_StateCTB_y", -1.e6);
      tuple->column( head+"_Matched_StateCTB_z", -1.e6);
  }

  if( assoc ){

    
    // -- Associate MC particle to the long track that was associated to the muonTT track.
    const LHCb::MCParticle* assocPartMC = NULL;
    const LHCb::MCParticle* probePartMC = NULL;
    if( m_MC && assocTrack ) assocPartMC = assocMCPart( assocTrack );
    if( m_MC ) probePartMC = assocMCPart( probeTrack );
    

    double sigma_pLong = -1000.0;
    double pChi2Long =  -1000.0; 
    double chi2NDoFLong = -1000.0;
    
    double deltaR = -1;
    double deltaP = -1;
    
    double deltaPhi = -1000.0;
    double deltaEta = -1000.0;

    double assocTrackPX = -1e10;
    double assocTrackPY = -1e10;
    double assocTrackPZ = -1e10;
    double assocTrackP = -1e10;
    double assocVeloChi2 = -1e10;
    double assocVeloNDoF = -1e10;
    double assocGhostProb = -1e10;

    int sameTrack = -1;
    bool probeMC = (probePartMC != NULL);
    bool assocMC = (assocPartMC != NULL);

    if ( assocPartMC && probePartMC){
        if (assocPartMC == probePartMC) sameTrack = 1;
        else sameTrack = 0;
    }

    //write additional information of the associated track
    if( assocTrack != NULL ){

  
      assocTrackPX = assocTrack->momentum().X();
      assocTrackPY = assocTrack->momentum().Y();
      assocTrackPZ = assocTrack->momentum().Z();
      assocTrackP = assocTrack->p();
      assocVeloChi2 = assocTrack->info(LHCb::Track::FitVeloChi2,-1);
      assocVeloNDoF = assocTrack->info(LHCb::Track::FitVeloNDoF,-1);
      assocGhostProb = assocTrack->ghostProbability();

      sigma_pLong = sqrt(assocTrack->firstState().errP2());
      pChi2Long =  assocTrack->probChi2();
      chi2NDoFLong = assocTrack->chi2()/assocTrack->nDoF();
     
      // ----------------------------------------------------------
      
      // -- Calculate Delta R between the probe track and the associated track
      deltaPhi = fabs( assocTrack->phi() - probeTrack->phi() );
      if(deltaPhi > M_PI) deltaPhi  = 2*M_PI-deltaPhi;
      deltaEta = assocTrack->pseudoRapidity() - probeTrack->pseudoRapidity();
      deltaR = sqrt( deltaPhi*deltaPhi + deltaEta*deltaEta );
      deltaP = assocTrack->p() - probeTrack->p();
      // -----------------------------------------------------------------
    
      
            
    }

    warning() << "Additional matched track info pulled" << endmsg;
    // -- And now fill the tuple...
    
    warning() << "Filling tuple" << endmsg;
    
    tuple->column(head+"_Matched_Px", assocTrackPX);
    tuple->column(head+"_Matched_Py", assocTrackPY);
    tuple->column(head+"_Matched_Pz", assocTrackPZ);
    tuple->column(head+"_Matched_P", assocTrackP);
    tuple->column(head+"_Matched_dP", sigma_pLong);
    tuple->column(head+"_Matched_probChi2", pChi2Long );
    tuple->column(head+"_Matched_Chi2NDoF", chi2NDoFLong);
    tuple->column(head+"_Matched_VeloChi2", assocVeloChi2);
    tuple->column(head+"_Matched_VeloNDoF", assocVeloNDoF);
    tuple->column(head+"_Matched_GhostProb", assocGhostProb);
    
    tuple->column(head+"_DeltaR", deltaR);
    tuple->column(head+"_DeltaP", deltaP);

    tuple->column(head+"_Matched_SameMCPart", sameTrack);
    tuple->column(head+"_MCPart", probeMC);
    tuple->column(head+"_Matched_MCPart", assocMC);
    
    if(m_MC && assocPartMC){
      tuple->column(head+"_MatchedMC_Px", assocPartMC->momentum().x());
      tuple->column(head+"_MatchedMC_Py", assocPartMC->momentum().y());
      tuple->column(head+"_MatchedMC_Pz", assocPartMC->momentum().z());
      tuple->column(head+"_MatchedMC_P", assocPartMC->p());
      tuple->column(head+"_MatchedMC_PID", assocPartMC->particleID().pid());
    }
    

  }
  

  warning() << "TupleToolTrackEffMatch done!" << endmsg;
  return StatusCode::SUCCESS ;
  
}
//=============================================================================
// Count what fractions of LHCbIDs are the same for two tracks in numerous subdetectors
//=============================================================================
void TupleToolTrackEffMatch::commonLHCbIDs(const LHCb::Track* longTrack, const LHCb::Track* probeTrack, const LHCb::MuonPID* longMuPID, double& muFraction, double& TTFraction, double& TFraction, double& ITFraction, double& OTFraction, double& VeloFraction){

  // -- Get the LHCbIDs of the Probe-track and fill them into containers for each subdetector
  std::vector<LHCb::LHCbID> probeTrackIDs = probeTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> probeTrackTTIDs;
  std::vector<LHCb::LHCbID> probeTrackTIDs;
  std::vector<LHCb::LHCbID> probeTrackITIDs;
  std::vector<LHCb::LHCbID> probeTrackOTIDs;
  std::vector<LHCb::LHCbID> probeTrackMuonIDs;
  std::vector<LHCb::LHCbID> probeTrackVeloIDs;
  
  std::vector<LHCb::LHCbID> longTrackIDs = longTrack->lhcbIDs();
  std::vector<LHCb::LHCbID> longTrackTTIDs;
  std::vector<LHCb::LHCbID> longTrackTIDs;
  std::vector<LHCb::LHCbID> longTrackITIDs;
  std::vector<LHCb::LHCbID> longTrackOTIDs;
  std::vector<LHCb::LHCbID> longTrackMuonIDs;
  if (longMuPID && longMuPID->muonTrack()) longTrackMuonIDs = longMuPID->muonTrack()->lhcbIDs();
  std::vector<LHCb::LHCbID> longTrackVeloIDs;


  
  // -- Check if the long track has TT hits
  for( std::vector<LHCb::LHCbID>::iterator it = longTrackIDs.begin() ; it != longTrackIDs.end() ; ++it){

    if (!std::is_sorted(longTrackIDs.begin(),longTrackIDs.end())) warning() << "long track ids sorted: " << std::is_sorted(longTrackIDs.begin(),longTrackIDs.end()) << endmsg;
    
    LHCb::LHCbID id = *it;
    if(id.isTT()){
      longTrackTTIDs.push_back(id);
    }
    if(id.isIT() || id.isOT()){
      longTrackTIDs.push_back(id);
    }
    if(id.isIT()){
      longTrackITIDs.push_back(id);
    }
    if(id.isOT()){
      longTrackOTIDs.push_back(id);
    }
    if(id.isVelo()){
      longTrackVeloIDs.push_back(id);
    }
    
  }
  
  // -- Sort LHCbIDs of probe track in subdetector IDs
  for( std::vector<LHCb::LHCbID>::iterator it = probeTrackIDs.begin() ; it != probeTrackIDs.end() ; ++it){

    LHCb::LHCbID id = (*it);
    if (!std::is_sorted(probeTrackIDs.begin(),probeTrackIDs.end())) warning() << "probe track ids sorted: " << std::is_sorted(probeTrackIDs.begin(),probeTrackIDs.end()) << endmsg;

    if(id.isMuon()) probeTrackMuonIDs.push_back(id);
    if(id.isTT()) probeTrackTTIDs.push_back(id);
    if(id.isIT() || id.isOT()) probeTrackTIDs.push_back(id);
    if(id.isIT()) probeTrackITIDs.push_back(id);
    if(id.isOT()) probeTrackOTIDs.push_back(id);
    if(id.isVelo()) probeTrackVeloIDs.push_back(id);

  }
  warning() << "Probe track muon hits: " << probeTrackMuonIDs.size() << endmsg;
  warning() << "Long track muon hits: " << longTrackMuonIDs.size() << endmsg;
  warning() << "Long track T hits: " << longTrackTIDs.size() << endmsg;
  warning() << "Long track TT hits: " << longTrackTTIDs.size() << endmsg;
  warning() << "Long track Velo hits: " << longTrackVeloIDs.size() << endmsg;
  
  // -- Count common hist
  int OverlapTT = nOverlap(probeTrackTTIDs, longTrackTTIDs, false);
  int OverlapT = nOverlap(probeTrackTIDs, longTrackTIDs, false);
  int OverlapIT = nOverlap(probeTrackITIDs, longTrackITIDs, false);
  int OverlapOT = nOverlap(probeTrackOTIDs, longTrackOTIDs, false);
  int OverlapVelo = nOverlap(probeTrackVeloIDs, longTrackVeloIDs, false);
  int OverlapMuon = nOverlap(probeTrackMuonIDs, longTrackMuonIDs, false);
 
  TTFraction = std::max(double(OverlapTT)/probeTrackTTIDs.size(),double(OverlapTT)/longTrackTTIDs.size()) ? (probeTrackTTIDs.size()!=0 && longTrackTTIDs.size()!=0) : 0.;
  TFraction = std::max(double(OverlapT)/probeTrackTIDs.size(),double(OverlapT)/longTrackTIDs.size()) ? (probeTrackTIDs.size()!=0 && longTrackTIDs.size()!=0) : 0.;
  ITFraction = std::max(double(OverlapIT)/probeTrackITIDs.size(),double(OverlapIT)/longTrackITIDs.size()) ? (probeTrackITIDs.size()!=0 && longTrackITIDs.size()!=0) : 0.;
  OTFraction = std::max(double(OverlapOT)/probeTrackOTIDs.size(),double(OverlapOT)/longTrackOTIDs.size()) ? (probeTrackOTIDs.size()!=0 && longTrackOTIDs.size()!=0) : 0.;
  VeloFraction = std::max(double(OverlapVelo)/probeTrackVeloIDs.size(),double(OverlapVelo)/longTrackVeloIDs.size()) ? (probeTrackVeloIDs.size()!=0 && longTrackVeloIDs.size()!=0) : 0.;
  muFraction = std::max(double(OverlapMuon)/probeTrackMuonIDs.size(),double(OverlapMuon)/longTrackMuonIDs.size()) ? (probeTrackMuonIDs.size()!=0 && longTrackMuonIDs.size()!=0) : 0.;

}

//=============================================================================
// Count overlapping LHCb IDs for two ID vectors
//=============================================================================
size_t TupleToolTrackEffMatch::nOverlap(const std::vector<LHCb::LHCbID> list1, const std::vector<LHCb::LHCbID> list2, bool verb){

  if (verb){
      warning() << "probe hits: " << list1.size() << endmsg;
      warning() << "long hits: " << list2.size() << endmsg;
  }
  size_t rc(0) ;
  std::vector<LHCb::LHCbID>::const_iterator first1 = list1.begin() ;
  std::vector<LHCb::LHCbID>::const_iterator last1  = list1.end() ;
  std::vector<LHCb::LHCbID>::const_iterator first2 = list2.begin() ;
  std::vector<LHCb::LHCbID>::const_iterator last2  = list2.end() ;
  while (first1 != last1 && first2 != last2)
  {
    if ( *first1 < *first2 )
    {
      ++first1;
    }
    else if ( *first2 < *first1 )
    {
      ++first2;
    }
    else 
    {
      ++first1;
      ++first2;
      ++rc ;
    }
  } 
  if (verb) {
      warning() << "# common hits: " << rc << endmsg;
      if (rc == 0) warning() << "NO COMMON HITS!" << endmsg;
  }
  return rc;
}

//=============================================================================
// Check if the associated track really belongs to a muon from the corresponding mother in MC
//=============================================================================
const LHCb::MCParticle* TupleToolTrackEffMatch::assocMCPart( const LHCb::Track* assocTrack ){
  

  LHCb::Particle::Range longTracks;

  // -- Warning: This is hardcoded!
  if(exist<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles")){
    longTracks = get<LHCb::Particle::Range>("Phys/StdAllLooseMuons/Particles");
  }else{
    if ( msgLevel(MSG::DEBUG) ) debug() << "Could not find container: " << "Phys/StdAllLooseMuons/Particles" << endmsg;
    return NULL;
  }
  
  const LHCb::Particle* assocPartMC = NULL;

  for(LHCb::Particle::Range::const_iterator it = longTracks.begin() ; it != longTracks.end() ; ++it){

    const LHCb::Particle* part = (*it);
    if( !part->proto() ) continue;
    if( !part->proto()->track() ) continue;

    const LHCb::Track* track = part->proto()->track();

    if( track == assocTrack){
      assocPartMC = part;
      break;
    }

  }
    
  if( assocPartMC == NULL) return NULL;
  
  const LHCb::MCParticle* mcp = NULL;
  if( assocPartMC->particleID().pid() != 0){
    mcp = m_p2mcAssoc->relatedMCP( assocPartMC );
  }
 
  

  return mcp;


}

