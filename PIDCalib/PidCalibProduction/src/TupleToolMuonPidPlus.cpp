
/** @class TupleToolMuonPidPlus TupleToolMuonPidPlus.cpp
 *
 *  @author Giacomo Graziani
 *  @date   2015-12-18
 *
 *  mu/pi PID BDT classifier for Ks->mumu
 *
 *  to run on Grid:
 *    - set option RunOnGrid to yes
 *    - copy the file $KS2MUMUTUPLESROOT/tmva/BDTontop/MuonTMVA_BDTv3.weights.xml
 *        to your input sandbox
 *
 */
#include <stdlib.h> 
// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "TupleToolMuonPidPlus.h"
#include <map>
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface

#include "MuonID/IMuonIDTool.h"
#include "Event/MuonPID.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/RecSummary.h"
#include "Event/ODIN.h"
#include "Event/RichPID.h"


// from TMVA
#include "TMVA/Tools.h"
#include "TMVA/Reader.h"
#include "TMVA/MethodCuts.h"


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TupleToolMuonPidPlus::TupleToolMuonPidPlus( const std::string& type,
                                    const std::string& name,
                                    const IInterface* parent )
  : TupleToolBase ( type, name , parent ),  m_TMVAreader(NULL)
{
  declareInterface<IParticleTupleTool>(this);
  declareProperty("MuonIDPlusToolName",m_MuonIDPlusToolName="MuonIDPlusTool");
  declareProperty("RunOnGrid", m_runOnGrid=false,
                  "search the BDT weight file in the local directory (for GRID jobs)");
}

StatusCode TupleToolMuonPidPlus::initialize() {
  const StatusCode sc = TupleToolBase::initialize();
  if ( sc.isFailure() ) return sc;

  m_newtool = tool<IMuonIDTool>(m_MuonIDPlusToolName, this);
  if(!m_newtool) {
    error() <<m_MuonIDPlusToolName<<" is needed!"<<endmsg;
    return StatusCode::FAILURE;
  }
  initTMVA();
  return sc;
}



void TupleToolMuonPidPlus::dumpInput() {
  const LHCb::ODIN* odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default);
  if ( !odin ) { odin = getIfExists<LHCb::ODIN>(evtSvc(),LHCb::ODINLocation::Default,false); }


  Printf("Run %d Event %lld",odin->runNumber(),odin->eventNumber());
  Printf("P %.8f PT %.8f Ntr %d",m_fP,m_fPt,int(m_fnTracks+0.01));
  for (int M=2; M<6; M++) 
    Printf("Station M%d  Match %d Sigma %.6f Iso %.6f Time %.3f",
           M, int(m_fMuMatch[M-1]+.01), m_fMuSigma[M-1], m_fMuIso[M-1], m_fMuTime[M-1]);
  Printf("Chi2Cag %.8f ",	 m_fChi2Cag);
  Printf("Track chi2/dof %.4f Velo %.4f Trk %.4f match %.4f ghostP %.4f lik %.4f",
         m_fTrChi2Ndof, m_fTrFitVeloChi2Ndof, m_fTrFitTChi2Ndof, m_fTrMatchChi2, m_fGhostProb, m_fTlik);
  Printf("Rich use gas 1 %d gas2 %d above Kthr %d DLLmu %.4f DLLk %.4f",
         (int) (m_fUsedRich1Gas+.001), (int) (m_fUsedRich2Gas+.001), (int) (m_fAboveKThr+.001),
         m_fRichDLLmu, m_fRichDLLk);
  Printf("Velo Charge  %.4f  Ecal E %.4f  Hcal E  %.4f",
         m_fVeloCharge, m_fEcalE, m_fHcalE);

}


void TupleToolMuonPidPlus::initTMVA() {
   // This loads the library
  TMVA::Tools::Instance();
  m_TMVAreader = new TMVA::Reader( "!Color:!Silent" );  
  m_TMVAreader->AddVariable( "P", &m_fP); 
  m_TMVAreader->AddVariable( "PT", &m_fPt); 
  m_TMVAreader->AddVariable( "nTracks",&m_fnTracks);
  m_TMVAreader->AddVariable( "MuMatch[1]", &(m_fMuMatch[1])); 
  m_TMVAreader->AddVariable( "MuMatch[2]", &(m_fMuMatch[2])); 
  m_TMVAreader->AddVariable( "MuMatch[3]", &(m_fMuMatch[3])); 
  m_TMVAreader->AddVariable( "MuMatch[4]", &(m_fMuMatch[4])); 
  m_TMVAreader->AddVariable( "MuSigma[1]", &(m_fMuSigma[1])); 
  m_TMVAreader->AddVariable( "MuSigma[2]", &(m_fMuSigma[2])); 
  m_TMVAreader->AddVariable( "MuSigma[3]", &(m_fMuSigma[3])); 
  m_TMVAreader->AddVariable( "MuSigma[4]", &(m_fMuSigma[4])); 
  m_TMVAreader->AddVariable( "MuIso[1]", &(m_fMuIso[1])); 
  m_TMVAreader->AddVariable( "MuIso[2]", &(m_fMuIso[2])); 
  m_TMVAreader->AddVariable( "MuIso[3]", &(m_fMuIso[3])); 
  m_TMVAreader->AddVariable( "MuIso[4]", &(m_fMuIso[4])); 
  m_TMVAreader->AddVariable( "Chi2Cag", &m_fChi2Cag);
  m_TMVAreader->AddVariable( "MuTime[1]", &(m_fMuTime[1]));
  m_TMVAreader->AddVariable( "MuTime[2]", &(m_fMuTime[2]));
  m_TMVAreader->AddVariable( "MuTime[3]", &(m_fMuTime[3]));
  m_TMVAreader->AddVariable( "MuTime[4]", &(m_fMuTime[4]));
  m_TMVAreader->AddVariable( "TrChi2Ndof",&m_fTrChi2Ndof);
  m_TMVAreader->AddVariable( "GhostProb",&m_fGhostProb);
  m_TMVAreader->AddVariable( "TrMatchChi2",&m_fTrMatchChi2);
  m_TMVAreader->AddVariable( "Tlik", &m_fTlik);
  m_TMVAreader->AddVariable( "TrFitVeloChi2Ndof",&m_fTrFitVeloChi2Ndof);
  m_TMVAreader->AddVariable( "TrFitTChi2Ndof",&m_fTrFitTChi2Ndof);
  m_TMVAreader->AddVariable( "UsedRich1Gas", &m_fUsedRich1Gas);
  m_TMVAreader->AddVariable( "UsedRich2Gas", &m_fUsedRich2Gas);
  m_TMVAreader->AddVariable( "AboveKThr", &m_fAboveKThr);
  m_TMVAreader->AddVariable( "RichDLLmu",&m_fRichDLLmu);
  m_TMVAreader->AddVariable( "RichDLLk",&m_fRichDLLk);
  m_TMVAreader->AddVariable( "VeloCharge",&m_fVeloCharge);
  m_TMVAreader->AddVariable( "EcalE",&m_fEcalE);
  m_TMVAreader->AddVariable( "HcalE",&m_fHcalE);
  
  std::string wfile="MuonTMVA_BDTv3.weights.xml";
  if(m_runOnGrid == false ) {
    wfile=getenv("PIDCALIBPRODUCTIONROOT");  
    wfile += "/tmva/BDTontop/MuonTMVA_BDTv3.weights.xml";
  }
  m_TMVAreader->BookMVA("BDT",wfile.data());
}




float TupleToolMuonPidPlus::CombDLL2BDT(double x) {
  // apply empirical transformation to CombDLL variable to give same probability of BDT for low energy pions
  return (float) (-0.0993107   +
                  0.0165849 * x  +
                  -0.000707536 * pow(x,2)  +
                  0.000109344 * pow(x,3)  +
                  6.44315e-06 * pow(x,4)  +
                  -2.26864e-07 * pow(x,5)) ;  
}


StatusCode TupleToolMuonPidPlus::fill( const LHCb::Particle*
                                       , const LHCb::Particle* P
                                       , const std::string& head
                                       , Tuples::Tuple& tuple )
{

  if (!P)  {
    info() << "TupleToolMuonPidPlus::fill called with null Particle pointer!"<<endmsg;
    return StatusCode::SUCCESS;
  }
  if ( !P->isBasicParticle() ) return StatusCode::SUCCESS; 
  // no PID info for composite!
  if ( isPureNeutralCalo(P)  ) return StatusCode::SUCCESS;
  // no PID information for calo neutrals
  
    
  const LHCb::ProtoParticle* proto = P->proto();
  if (!proto) {
    info() << "TupleToolGGMu::fill called with null ProtoParticle pointer!"<<endmsg;
    return StatusCode::SUCCESS;
  }
  const LHCb::Track *pTrack=proto->track();
  if (!pTrack) return StatusCode::SUCCESS;
  
  
  const LHCb::RecSummary * rS = 
    getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default);
  if ( !rS ) 
  {
    rS = getIfExists<LHCb::RecSummary>(evtSvc(),LHCb::RecSummaryLocation::Default,false); 
  }


  // variables to be used in BDT
  // note that input variables for TMVA Reader must be float for some reason 
  
  m_fP=pTrack->p();
  m_fPt=pTrack->pt();
  m_fnTracks = (float) ( rS ? rS->info(LHCb::RecSummary::nTracks,-1)           : -2) ;

  m_fTrChi2Ndof = (pTrack->nDoF() >0) ? ( pTrack->chi2()/pTrack->nDoF() ) : -1.;
  m_fGhostProb  = pTrack->ghostProbability() ;
  m_fTrMatchChi2 = pTrack->info( LHCb::Track::FitMatchChi2, -1 );
  m_fTlik  = pTrack->likelihood();
  m_fTrFitVeloChi2Ndof = ( pTrack->info(LHCb::Track::FitVeloNDoF,0) > 0 ) ?
    ( pTrack->info(LHCb::Track::FitVeloChi2, -1.)/pTrack->info(LHCb::Track::FitVeloNDoF, 0) )
    : -1.;
  m_fTrFitTChi2Ndof = ( pTrack->info(LHCb::Track::FitTNDoF,0) > 0 ) ?
    ( pTrack->info(LHCb::Track::FitTChi2, -1.)/pTrack->info(LHCb::Track::FitTNDoF, 0) )
    : -1.;

  const LHCb::RichPID* richPID = proto->richPID();
  m_fUsedRich1Gas = richPID ? ((float) richPID->usedRich1Gas()) : 0.; 
  m_fUsedRich2Gas = richPID ? ((float) richPID->usedRich2Gas()) : 0.; 
  m_fAboveKThr = richPID ? ((float) richPID->kaonHypoAboveThres()) : 0.; 
  m_fRichDLLmu = proto->info(LHCb::ProtoParticle::RichDLLmu,-1000);
  m_fRichDLLk = proto->info(LHCb::ProtoParticle::RichDLLk,-1000);
  m_fVeloCharge = proto->info(LHCb::ProtoParticle::VeloCharge,-10000.);
  m_fEcalE = proto->info(LHCb::ProtoParticle::CaloEcalE,-10000.);
  m_fHcalE = proto->info(LHCb::ProtoParticle::CaloHcalE,-10000.);

  LHCb::MuonPID* pid=m_newtool->getMuonID(pTrack);

  for (int s=0;s<MAXMUSTATIONS;s++) {    
    m_fMuMatch[s] = m_newtool->muonIDPropertyI(pTrack, "match", s);
    if(m_fMuMatch[s] == 2) m_fMuMatch[s] = 1 + m_newtool->muonIDPropertyI(pTrack, "clusize", s);
    m_fMuSigma[s]=m_newtool->muonIDPropertyD(pTrack, "matchSigma", s); 
    m_fMuIso[s]=m_newtool->muonIDPropertyD(pTrack, "iso", s);
    m_fMuTime[s] = m_newtool->muonIDPropertyD(pTrack, "time", s);
  }


  int ndof=0;
  m_fChi2Cag = (150.-0.000001);
  const LHCb::Track *mTrack= (pid ? pid->muonTrack() : NULL);
  if(mTrack) {
    ndof=mTrack->nDoF();
    if(ndof>0)
      m_fChi2Cag = TMath::Min( m_fChi2Cag, (float) (mTrack->chi2()/ndof) );
  }

  double myBDT=-0.5;
  
  if ( msgLevel(MSG::VERBOSE) ) dumpInput();
  if (m_fP < 3.*Gaudi::Units::GeV) { // BDT not valid in this region, use CombDLLMu with appropriate transformation
    double CombDLLMu = TMath::Max(proto->info(LHCb::ProtoParticle::CombDLLmu,-1000), -17.+0.000001);
    myBDT=CombDLL2BDT(CombDLLMu);
  }
  else { // BDT can be computed
    myBDT=TMath::Max(-0.5, m_TMVAreader->EvaluateMVA("BDT"));
  }
  debug() << "MuIDFiCa says "<<myBDT<<endmsg;


  // fill tuple
  const std::string prefix=fullName(head);

  tuple->column( prefix+"_muonIDPlusBDT", myBDT);

  if(isVerbose() ) { // add all the details
    tuple->array( prefix+"_muPmatchM", m_fMuMatch, MAXMUSTATIONS);
    tuple->array( prefix+"_muPmatchS", m_fMuSigma, MAXMUSTATIONS);
    tuple->array( prefix+"_muPisoM", m_fMuIso, MAXMUSTATIONS);
    tuple->array( prefix+"_muPtime", m_fMuIso, MAXMUSTATIONS);
    tuple->column( prefix+"_muPchi2", m_fChi2Cag);
  }
    
  return StatusCode::SUCCESS;
}




DECLARE_TOOL_FACTORY( TupleToolMuonPidPlus )
