// $Id: ErasePidVariables.h
#ifndef ERASEPIDVARIABLES_H
#define ERASEPIDVARIABLES_H 1

// Include files
// from DaVinci, this is a specialized GaudiAlgorithm
#include "Kernel/DaVinciTupleAlgorithm.h"


 class ErasePidVariables : public DaVinciTupleAlgorithm {
 public:
   ErasePidVariables( const std::string& name, ISvcLocator* pSvcLocator );

   virtual ~ErasePidVariables( );

   StatusCode initialize() override;
   StatusCode execute   () override;
   StatusCode finalize  () override;

 protected:

 private:
   std::string m_protoPath;
   bool m_eraseRich, m_eraseMuon, m_eraseComb;
 };
#endif // ERASEPIDVARIABLES_H
