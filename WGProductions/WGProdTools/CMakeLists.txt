################################################################################
# Package: WGProdTools
################################################################################
gaudi_subdir(WGProdTools v1r0)

gaudi_depends_on_subdirs(GaudiPolicy                           
                         GaudiKernel   
                         Event/RecEvent      
                         Phys/DecayTreeTuple
                         Phys/DecayTreeTupleBase
                         Phys/LoKiPhys      
                         Phys/LoKi          
                         Det/Magnet        
                         Phys/DecayTreeFitter
                         Phys/LoKiArrayFunctors
                         Event/HltEvent
                         Phys/TeslaTools
                         Muon/MuonID
                         Muon/MuonTrackRec
                         Rec/RecInterfaces
                         Event/RecEvent
                        )
  
       

find_package(Boost)
find_package(ROOT COMPONENTS Hist Gpad Graf Matrix TreePlayer MathCore MathMore SPlot Minuit TMVA)
find_package(GSL)

find_package(HepMC)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(WGProdTools
                 src/*.cpp
                 INCLUDE_DIRS HepMC Boost ROOT GSL
                 LINK_LIBRARIES ROOT GSL GaudiAlgLib GaudiKernel LHCbKernel RecEvent TrackEvent CaloUtils CaloDetLib DecayTreeTupleBaseLib LoKiPhysLib LoKiArrayFunctorsLib HltEvent)

gaudi_install_python_modules()
