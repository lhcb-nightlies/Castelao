from config.Turbo02   import config as configureTurbo02
from config.Turbo03   import config as configureTurbo03
from config.Turbo04   import config as configureTurbo04
from config.Stripping import config as configureStripping


def defineConfiguration (
      configuration_key ,
      **kwargs
    ):

  if configuration_key == "Turbo02":
    configureTurbo02 ( **kwargs )
  elif configuration_key == "Turbo03":
    configureTurbo03 ( **kwargs )
  elif configuration_key == "Turbo04":
    configureTurbo04 ( **kwargs )
  elif configuration_key == "Stripping":
    configureStripping ( **kwargs )
  else:
    raise KeyError ( "Unknown configuration key %s" % configuration_key );

