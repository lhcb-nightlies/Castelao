################################################################################
#### Configures the DST Writer
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

from DSTWriters.microdstelements import CloneHltDecReports

def config ( outputStreamName ):

  # Default Configuration of SelDSTWriter
  SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
  SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = False ) }


  SelDSTWriterConf['default'].extraItems.append("/Event/Rec/Summary#99")

  SelDSTWriterConf['default'].extraItems += [
    '/Event/Hlt1/DecReports#99'
    '/Event/Hlt2/DecReports#99'
  #  '/Event/Hlt1/SelReports#99'
  #  '/Event/Hlt2/SelReports#99'
  ]

  SelDSTWriterElements ['default'] += [
      CloneHltDecReports(locations=["/Event/Hlt1/DecReports", "/Event/Hlt2/DecReports"]),
    ]


  from WGProdTools import WGProdWriter
  writerName = 'WG' + outputStreamName
  WGProdWriter (writerName).RootInTES = "/Event"


  ################################################################################
  ## Instance of the DSTWriter and its sequence                                 ##
  ################################################################################

  uDstWriter = SelDSTWriter(
      "MyDSTWriter"                               ,
      StreamConf         =  SelDSTWriterConf      ,
      MicroDSTElements   =  SelDSTWriterElements  ,
    )

  from Configurables import DaVinci
  DaVinci.UserAlgorithms = [ uDstWriter.sequence() ]


  from Configurables import RootCnvSvc
  RootCnvSvc().GlobalCompression = "LZMA:6"


  ################################################################################
  ## DaVinci configuration (ROOT in TES, not Simulation, Lumi defined)
  DaVinci (
      InputType       = 'DST'            ,
      Simulation      = False            ,
      PrintFreq       = 5                ,
      Lumi            = True             ,
    )

