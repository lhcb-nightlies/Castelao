// from Gaudi
#include "GaudiKernel/AlgFactory.h" 

// local
#include "TurboFixByMatching.h"

// ROOT
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"

//-----------------------------------------------------------------------------
// Implementation file for class : TurboFixByMatching
//
// 2017-12-12 : Lucio Anderlini        
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( TurboFixByMatching )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TurboFixByMatching::TurboFixByMatching( const std::string& name,
    ISvcLocator* pSvcLocator)
  : DaVinciTupleAlgorithm ( name , pSvcLocator )
{}
//=============================================================================
// Destructor
//=============================================================================
TurboFixByMatching::~TurboFixByMatching() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode TurboFixByMatching::initialize() {
  StatusCode sc = DaVinciTupleAlgorithm::initialize(); 
  if ( sc.isFailure() ) return sc;

  if (msgLevel(MSG::DEBUG)) debug() << "==> Initialize" << endmsg;

  m_matcher = tool<ITeslaMatcher> ( m_matcherToolName, this );
  sc = Gaudi::Utils::setProperty ( m_matcher, "CheckPID", false);
  if ( sc.isFailure() ) return sc;

  return StatusCode::SUCCESS;
}
//=============================================================================
// Main execution
//=============================================================================
StatusCode TurboFixByMatching::execute() 
{
  if (msgLevel(MSG::DEBUG)) debug() << "==> Execute" << endmsg;

  setFilterPassed(true);  // Mandatory. Set to true if event is accepted.

  // cannot use particles() because I don't want constantness
  LHCb::Particles* particles = getIfExists<LHCb::Particles>(m_inputParticles);

  if (!particles)
    return StatusCode::SUCCESS;

  for (auto p : *particles)
    fixDaughters ( p );

 
  return StatusCode::SUCCESS;
}

//=============================================================================
// Fix daughters
//=============================================================================
StatusCode TurboFixByMatching::fixDaughters ( LHCb::Particle *parent )
{
  if (!parent) return StatusCode::SUCCESS;

  auto daughters = parent->daughters();
  for (auto p : daughters)
    if (p && p -> isBasicParticle() == false)
      fixDaughters ( p ).ignore();

  for (auto daughter : daughters )
  {
    if (!daughter)
    {
      Warning ( "Invalid daughters for particle" + parent->particleID().toString(),
          StatusCode::SUCCESS ).ignore();
      continue;
    }
    const LHCb::Particle *best_match = daughter;

    if ( daughter -> isBasicParticle() )
    {
      StatusCode sc = m_matcher->findBestMatch (daughter, best_match, m_targetTes);
      if ( sc.isFailure() || !best_match )
      {
        Warning("Matching failed", sc).ignore();
        continue;
      }

      if (daughter->parent() == best_match->parent())
        continue;

      if (!best_match->proto())
      {
        Warning("Matched particle has no protoparticle",
            StatusCode::SUCCESS).ignore();
        continue;
      }
      daughter->setProto ( best_match->proto() );
    }
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode TurboFixByMatching::finalize() {

  info() << "==> Finalize" << endmsg;
  if (msgLevel(MSG::DEBUG)) debug() << "==> Finalize" << endmsg;

  return DaVinciTupleAlgorithm::finalize(); 
} 


