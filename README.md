# The CASTELAO project

Castelao is a versatile collection of packages used by various working groups. New versions are released on demand by the package maintainers.
The structure is rather free. It depends on [DaVinci](https://gitlab.cern.ch/lhcb/DaVinci).

If you are not interested in those dependencies, and rather prefer to use latest heptools, please consider using [Urania](https://gitlab.cern.ch/lhcb/Urania) instead. If you are interested instead of an additional dependency on [Bender](https://gitlab.cern.ch/lhcb/Bender), please consider using [Erasmus](https://gitlab.cern.ch/lhcb/Erasmus) instead.

## How to work with Castelao

For development purposes of any of the packages hosted under Castelao, please follow the [instructions](https://twiki.cern.ch/twiki/bin/view/LHCb/Git4LHCb#Satellite_projects) at the Git4LHCb twiki.

This project is **only** available for gcc62 platforms (x86_64-slc6-gcc62-opt).

In any case, there are two important rules you must remember:

1. <b>Never clone the whole project for development purposes.</b> Please follow the instructions above.
2. <b>Always checkout the packages from the master branch.</b> If not, when asking for a Merge Request (MR), incompatibilities will appear and thus the MR will be closed. 

Only project maintainers are allowed to accept and revert MR.

## Mailing list

For issues, questions and problems; please send an e-mail to:

[<b>lhcb-castelao-developers@cern.ch</b>](mailto:lhcb-castelao-developers@cern.ch)

## About 

Alfonso Daniel Rodríguez Castelao (30 January 1886 – 7 January 1950), commonly known as Castelao, was a Galician writer, painter, doctor and politician. He was one of the most important promoters of the Galician identity and culture, and was one of the main names behind the cultural and literary movement _Xeración Nós_. His alma mater was the University of Santiago de Compostela.  [(+ info)](https://en.wikipedia.org/wiki/Alfonso_Daniel_Rodr%C3%ADguez_Castelao). 

