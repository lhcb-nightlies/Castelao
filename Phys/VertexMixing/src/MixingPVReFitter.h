#ifndef MIXINGPVREFITTER_H
#define MIXINGPVREFITTER_H 1

// Include files
// from Gaudi
#include <GaudiAlg/GaudiTool.h>

// DaVinci
#include <Kernel/IPVReFitter.h>

// Event
#include <Event/RecVertex.h>

class IIncidentSvc;

/** @class MixingPVReFitter MixingPVReFitter.h
 *
 *  A tool to forward refitting a PV to another tool and moving the vertex afterwards.
 *
 *  @author Roel Aaij
 *  @date   2011-11-24
 */
class MixingPVReFitter : public extends<GaudiTool, IPVReFitter> {
public:

   using base_class::base_class;

   StatusCode initialize() override;

   /// refit PV
   StatusCode reFit(LHCb::VertexBase* PV) const override;

   /// remove track used for a (B) LHCb::Particle and refit PV
   StatusCode remove(const LHCb::Particle* part,
                     LHCb::VertexBase* PV) const override;

private:

   Gaudi::Property<std::string> m_refitterName{this, "PVReFitter", "LoKi::PVReFitter:PUBLIC"};
   Gaudi::Property<int> m_infoKey{this, "ExtraInfoKey", 42};

   SmartIF<IPVReFitter> m_refitter;

};
#endif // PVREFITTER_H
