#ifndef PVMIXER_H
#define PVMIXER_H 1

#include <GaudiKernel/IIncidentListener.h>
#include <GaudiAlg/GaudiAlgorithm.h>

#include <Event/RecVertex.h>

/** @class PVMixer PVMixer.h
 *
 *
 *  @author Roel Aaij
 *  @date   2012-09-18
 */
class GAUDI_API PVMixer : public extends<GaudiAlgorithm, IIncidentListener> {
public:

   using base_class::base_class;

   StatusCode initialize() override;
   StatusCode execute() override;

   void handle(const Incident&) override;

private:

   // Properties
   Gaudi::Property<std::string> m_pvInputLocation{this, "PVInputLocation", LHCb::RecVertexLocation::Primary};
   Gaudi::Property<std::string> m_pvOutputLocation{this, "PVOutputLocation", "Rec/Vertex/Mixed"};
   Gaudi::Property<unsigned int> m_waitEvents{this, "WaitEvents", 1};
   Gaudi::Property<int> m_infoKey{this, "ExtraInfoKey", 42};

   // Services
   SmartIF<IIncidentSvc> m_incidentSvc;

   // Data members
   unsigned int m_waited = 0;
   std::list<std::unique_ptr<LHCb::RecVertex>> m_mixedVertices;

};

#endif // PVMIXER_H
