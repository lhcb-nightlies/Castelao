LOCOUT=/data/lhcb/users/hill/gangadir/workspace/dhill/LocalXML/425/tuple_sum/
DIR1=Bd2DsttaunuWSTuple/DecayTree
DIR2=Bd2DsttaunuNonPhysTuple/DecayTree
DIR3=Bd2DsttaunuTuple/DecayTree

for path in "$@"
do
	fname=$(basename $path .root)
	passname=$fname"_BothBDT.root"
	root -b "AddBothBDTtoNtuple.C(\"$path\",\"$LOCOUT/$passname\",\"$DIR1\")"
	root -b "AddBothBDTtoNtuple.C(\"$path\",\"$LOCOUT/$passname\",\"$DIR2\")"
	root -b "AddBothBDTtoNtuple.C(\"$path\",\"$LOCOUT/$passname\",\"$DIR3\")"
done
