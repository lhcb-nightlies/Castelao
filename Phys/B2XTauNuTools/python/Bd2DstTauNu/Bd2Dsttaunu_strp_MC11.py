import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from Configurables import (
		DaVinci,
		EventSelector,
		EventCountHisto,
		CheckPV,
		)



## GLOBAL OUTPUT LEVEL
outputLevel = INFO

#Run stripping on the MC
importOptions( "$B2XTAUNUTOOLSROOT/python/B2XTauNuTools/SelBd2Dsttaunu_ConfigureStripping_MC.py")

## SEQUENCER - As per the old .opts files.
SeqSelBd2Dsttaunu = GaudiSequencer("SeqSelBd2Dsttaunu")
SeqSelBd2Dsttaunu.OutputLevel = outputLevel

# Events in
SeqSelBd2DsttaunuInputCount = EventCountHisto("SeqSelBd2DsttaunuInputCount")
SeqSelBd2DsttaunuInputCount.OutputLevel = outputLevel
SeqSelBd2Dsttaunu.Members.append(SeqSelBd2DsttaunuInputCount)


#Events passing Selection
SeqSelBd2DsttaunuAfterSelection = EventCountHisto("SeqSelBd2DsttaunuAfterSelection")
SeqSelBd2DsttaunuAfterSelection.OutputLevel = outputLevel
SeqSelBd2Dsttaunu.Members.append(SeqSelBd2DsttaunuAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DstTauNu_MC/TupleBd2Dsttaunu_MC.py" )

#Events filled to ntuple
SeqSelBd2DsttaunuOutputCount = EventCountHisto("SeqSelBd2DsttaunuOutputCount")
SeqSelBd2DsttaunuOutputCount.OutputLevel = outputLevel
SeqSelBd2Dsttaunu.Members.append(SeqSelBd2DsttaunuOutputCount)
SeqSelBd2Dsttaunu.OutputLevel = outputLevel

## SEQUENCER - As per the old .opts files.
SeqSelBd2DsttaunuWS = GaudiSequencer("SeqSelBd2DsttaunuWS")
SeqSelBd2DsttaunuWS.OutputLevel = outputLevel

# Events in
SeqSelBd2DsttaunuWSInputCount = EventCountHisto("SeqSelBd2DsttaunuWSInputCount")
SeqSelBd2DsttaunuWSInputCount.OutputLevel = outputLevel
SeqSelBd2DsttaunuWS.Members.append(SeqSelBd2DsttaunuWSInputCount)


#Events passing Selection
SeqSelBd2DsttaunuWSAfterSelection = EventCountHisto("SeqSelBd2DsttaunuWSAfterSelection")
SeqSelBd2DsttaunuWSAfterSelection.OutputLevel = outputLevel
SeqSelBd2DsttaunuWS.Members.append(SeqSelBd2DsttaunuWSAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DstTauNu_MC/TupleBd2DsttaunuWS_MC.py" )

#Events filled to ntuple
SeqSelBd2DsttaunuWSOutputCount = EventCountHisto("SeqSelBd2DsttaunuWSOutputCount")
SeqSelBd2DsttaunuWSOutputCount.OutputLevel = outputLevel
SeqSelBd2DsttaunuWS.Members.append(SeqSelBd2DsttaunuWSOutputCount)
SeqSelBd2DsttaunuWS.OutputLevel = outputLevel


## SEQUENCER - As per the old .opts files.
SeqSelBd2DsttaunuNonPhys = GaudiSequencer("SeqSelBd2DsttaunuNonPhys")
SeqSelBd2DsttaunuNonPhys.OutputLevel = outputLevel

# Events in
SeqSelBd2DsttaunuNonPhysInputCount = EventCountHisto("SeqSelBd2DsttaunuNonPhysInputCount")
SeqSelBd2DsttaunuNonPhysInputCount.OutputLevel = outputLevel
SeqSelBd2DsttaunuNonPhys.Members.append(SeqSelBd2DsttaunuNonPhysInputCount)


#Events passing Selection
SeqSelBd2DsttaunuNonPhysAfterSelection = EventCountHisto("SeqSelBd2DsttaunuNonPhysAfterSelection")
SeqSelBd2DsttaunuNonPhysAfterSelection.OutputLevel = outputLevel
SeqSelBd2DsttaunuNonPhys.Members.append(SeqSelBd2DsttaunuNonPhysAfterSelection)

importOptions( "$B2XTAUNUTOOLSROOT/python/Bd2DstTauNu_MC/TupleBd2DsttaunuNonPhys_MC.py" )

#Events filled to ntuple
SeqSelBd2DsttaunuNonPhysOutputCount = EventCountHisto("SeqSelBd2DsttaunuNonPhysOutputCount")
SeqSelBd2DsttaunuNonPhysOutputCount.OutputLevel = outputLevel
SeqSelBd2DsttaunuNonPhys.Members.append(SeqSelBd2DsttaunuNonPhysOutputCount)
SeqSelBd2DsttaunuNonPhys.OutputLevel = outputLevel


from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"


#Event tuple to count how many events were processed
from Configurables import EventTuple, TupleToolTrigger
etuple=EventTuple()
etuple.ToolList = ["TupleToolEventInfo", "TupleToolPrimaries"]

#FOR MC11a:
DaVinci().CondDBtag = "sim-20111111-vc-md100"   #u for up, d for down in MC11
DaVinci().DDDBtag = "MC11-20111102"
DaVinci().EvtMax=-1
DaVinci().DataType='2011'
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().TupleFile = 'DVNtuple.root'
DaVinci().HistogramFile = 'DVHistos.root'
DaVinci().UserAlgorithms = [ SeqSelBd2Dsttaunu,  SeqSelBd2DsttaunuWS, SeqSelBd2DsttaunuNonPhys, etuple ]


