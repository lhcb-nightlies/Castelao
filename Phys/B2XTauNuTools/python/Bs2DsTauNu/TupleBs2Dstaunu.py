import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() will make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(
    os.path.abspath(
        os.path.split(
            inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO 

SeqTupleBs2Dstaunu = GaudiSequencer("SeqTupleBs2Dstaunu")
TupleBs2Dstaunu = DecayTreeTuple("Bs2DstaunuTuple")
TupleBs2Dstaunu.OutputLevel = outputLevel
TupleBs2Dstaunu.Inputs = ["Phys/Bs2DsTauNuForB2XTauNu/Particles"]

TupleBs2Dstaunu.Decay = "[(B_s0 -> ^(D_s- -> ^K- ^K+ ^pi-) ^(tau+ -> ^pi+ ^pi- ^pi+ ))]CC"
TupleBs2Dstaunu.Branches = {
    "Bs": "[(B_s0 -> (D_s- -> K- K+ pi-) (tau+ -> pi+ pi- pi+))]CC",
    "Ds": "[(B_s0 -> ^(D_s- -> K- K+ pi-) (tau+ -> pi+ pi- pi+))]CC",
    "ds_km": "[(B_s0 -> (D_s- -> ^K- K+ pi-) (tau+ -> pi+ pi- pi+))]CC",
    "ds_kp": "[(B_s0 -> (D_s- -> K- ^K- pi-) (tau+ -> pi+ pi- pi+))]CC",
    "ds_pion": "[(B_s0 -> (D_s- -> K- K+ ^pi-) (tau+ -> pi+ pi- pi+))]CC",
    "tau": "[(B_s0 -> (D_s- -> K- K+ pi-) ^(tau+ -> pi+ pi- pi+))]CC",
    "tau_pion": "[(B_s0 -> (D_s- -> K- K+ pi-) (tau+ -> ^pi+ ^pi- ^pi+))]CC"
}

TupleBs2Dstaunu.addTool(TupleToolDecay, name='Bs')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='Ds')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='ds_km')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='ds_kp')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='ds_pion')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='tau')
TupleBs2Dstaunu.addTool(TupleToolDecay, name='tau_pion')

FinalStateConfig(TupleBs2Dstaunu.ds_km)
FinalStateConfig(TupleBs2Dstaunu.ds_kp)
FinalStateConfig(TupleBs2Dstaunu.ds_pion)
FinalStateConfig(TupleBs2Dstaunu.tau_pion)

DalitzConfig(TupleBs2Dstaunu.tau)
DalitzConfig(TupleBs2Dstaunu.Ds)

IsoConfig(TupleBs2Dstaunu.tau)
IsoConfig(TupleBs2Dstaunu.Bs)
# LifetimeConfig(TupleBs2Dstaunu.tau)

ParentConfig(TupleBs2Dstaunu.tau)
ParentConfig(TupleBs2Dstaunu.Ds)
ParentConfig(TupleBs2Dstaunu.Bs)

CommonConfig(TupleBs2Dstaunu.Bs)
CommonConfig(TupleBs2Dstaunu.Ds)
CommonConfig(TupleBs2Dstaunu.ds_kp)
CommonConfig(TupleBs2Dstaunu.ds_km)
CommonConfig(TupleBs2Dstaunu.ds_pion)
CommonConfig(TupleBs2Dstaunu.tau)
CommonConfig(TupleBs2Dstaunu.tau_pion)
# Kinematic fit is using TupleToolB2XTauNuFit
# which is not used in the analysis
# which causes bugs with Bs2Dstaunu events
#KinematicFitConfig(TupleBs2DstaunuWS.B_s0,isMC)

DTFConfig(TupleBs2Dstaunu.Bs, 'D_s-')

TISTOSToolConfig(TupleBs2Dstaunu.Bs)
TupleToolConfig(TupleBs2Dstaunu)

TupleToolIsoGenericConfig(TupleBs2Dstaunu)

GaudiSequencer("SeqTupleBs2Dstaunu").Members.append(TupleBs2Dstaunu)
SeqTupleBs2Dstaunu.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBs2Dstaunu").Members.append(SeqTupleBs2Dstaunu)

