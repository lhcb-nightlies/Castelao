import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBu2D0taunu = GaudiSequencer("SeqTupleBu2D0taunu")
TupleBu2D0taunu = DecayTreeTuple("Bu2D0taunuTuple")
TupleBu2D0taunu.OutputLevel = outputLevel
#The decfile had a typo - the D0 goes DCS to K+ pi-. Will be fixed in the new MC11 processing.
TupleBu2D0taunu.Inputs = ["/Event/Phys/Bu2D0TauNuForB2XTauNu/Particles"]
TupleBu2D0taunu.Decay = "[B- -> (^D0 -> ^K+ ^pi-) (^tau- -> ^pi- ^pi+ ^pi- )]cc"
TupleBu2D0taunu.Branches = {
    "B" : "[B-]cc : [B- -> (D0 -> K+ pi-) (tau- -> pi- pi+ pi-)]cc",
    "tau_pion" : "[B- -> (D0 -> K+ pi-) (tau- -> ^pi- ^pi+ ^pi-)]cc",
    "d0_kaon" : "[B- -> (D0 -> ^K+ pi-) (tau- -> pi- pi+ pi-)]cc",
    "d0_pion" : "[B- -> (D0 -> K+ ^pi-) (tau- -> pi- pi+ pi-)]cc",
    "tau" : "[B- -> (D0 -> K+ pi-) (^tau- -> pi- pi+ pi-)]cc",
    "D0" : "[B- -> (^D0 -> K+ pi-) (tau- -> pi- pi+ pi-)]cc"}


TupleBu2D0taunu.addTool(TupleToolDecay, name = 'tau_pion')
TupleBu2D0taunu.addTool(TupleToolDecay, name = 'd0_pion')
TupleBu2D0taunu.addTool(TupleToolDecay, name = 'd0_kaon')
TupleBu2D0taunu.addTool(TupleToolDecay, name = 'tau')
TupleBu2D0taunu.addTool(TupleToolDecay, name = 'D0')
TupleBu2D0taunu.addTool(TupleToolDecay, name = 'B')


FinalStateConfig(TupleBu2D0taunu.tau_pion)
FinalStateConfig(TupleBu2D0taunu.d0_pion)
FinalStateConfig(TupleBu2D0taunu.d0_kaon)
#DalitzConfig(TupleBu2D0taunu.tau)
LifetimeConfig(TupleBu2D0taunu.tau)
ParentConfig(TupleBu2D0taunu.tau)
ParentConfig(TupleBu2D0taunu.D0)
ParentConfig(TupleBu2D0taunu.B)
CommonConfig(TupleBu2D0taunu.tau_pion)
CommonConfig(TupleBu2D0taunu.d0_pion)
CommonConfig(TupleBu2D0taunu.tau)
CommonConfig(TupleBu2D0taunu.D0)
CommonConfig(TupleBu2D0taunu.B)
CommonConfig(TupleBu2D0taunu.d0_kaon)
TISTOSToolConfig(TupleBu2D0taunu.B)
TupleToolConfig(TupleBu2D0taunu)
TupleToolMCConfig(TupleBu2D0taunu)

GaudiSequencer("SeqTupleBu2D0taunu").Members.append(TupleBu2D0taunu)

SeqTupleBu2D0taunu.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBu2D0taunu").Members.append(SeqTupleBu2D0taunu)

