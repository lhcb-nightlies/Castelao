dv = DaVinci()
dv.version = 'v33r2'
dv.platform = 'x86_64-slc5-gcc46-opt'
base=dv.user_release_area+'/DaVinci_'+dv.version+'/Phys/B2XTauNuTools/python/Bu2D0TauNu_MC/'
dv.optsfile = [base+'Bu2D0taunu_strp_MC10.py']

myJobs = [
	 ['Bd2DXMC10_withBu2D0TauNuSel','MCMC1011960000Beam3500GeV-Oct2010-MagUp-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Bu2DXMC10_withBu2D0TauNuSel','MCMC1012960000Beam3500GeV-Oct2010-MagUp-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Bs2DXMC10_withBu2D0TauNuSel','MCMC1013960000Beam3500GeV-Oct2010-MagUp-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
	,['Lb2DXMC10_withBu2D0TauNuSel','MCMC1015960000Beam3500GeV-Oct2010-MagUp-Nu2,5Sim01Trig0x002e002aFlaggedReco08Stripping12FlaggedSTREAMSDST.py']
]

for i in myJobs:	

	j = Job (name=i[0],application=dv)

	j.backend          = Dirac()
	j.inputdata        = dv.readInputData(base+i[1])
	j.splitter         = SplitByFiles(filesPerJob=10)
	j.outputfiles      = ['DVNtuple.root','DVHistos.root']
	j.postprocessors   = RootMerger(overwrite=True,ignorefailed=True,files=['DVNtuple.root'])
	j.do_auto_resubmit = True
	j.submit()
