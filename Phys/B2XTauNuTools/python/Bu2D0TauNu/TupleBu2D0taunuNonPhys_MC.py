import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *

import os, sys, inspect
# realpath() with make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile( inspect.currentframe() ))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

from B2XTauNuTools.TupleHelper import *
outputLevel = INFO

SeqTupleBu2D0taunuNonPhys = GaudiSequencer("SeqTupleBu2D0taunuNonPhys")
TupleBu2D0taunuNonPhys = DecayTreeTuple("Bu2D0taunuNonPhysTuple")
TupleBu2D0taunuNonPhys.OutputLevel = outputLevel
TupleBu2D0taunuNonPhys.Inputs = ["/Event/Phys/Bu2D0TauNuNonPhysTauForB2XTauNu/Particles"]
TupleBu2D0taunuNonPhys.Decay = "[B- -> (^D0 -> ^K+ ^pi-) (^tau- -> ^pi- ^pi- ^pi- )]cc"
TupleBu2D0taunuNonPhys.Branches = {
    "B" : "[B-]cc : [B- -> (D0 -> K+ pi-) (tau- -> pi- pi- pi-)]cc",
    "tau_pion" : "[B- -> (D0 -> K+ pi-) (tau- -> ^pi- ^pi- ^pi-)]cc",
    "d0_kaon" : "[B- -> (D0 -> ^K+ pi-) (tau- -> pi- pi- pi-)]cc",
    "d0_pion" : "[B- -> (D0 -> K+ ^pi-) (tau- -> pi- pi- pi-)]cc",
    "tau" : "[B- -> (D0 -> K+ pi-) (^tau- -> pi- pi- pi-)]cc",
    "D0" : "[B- -> (^D0 -> K+ pi-) (tau- -> pi- pi- pi-)]cc"}


TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'tau_pion')
TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'd0_pion')
TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'd0_kaon')
TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'tau')
TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'D0')
TupleBu2D0taunuNonPhys.addTool(TupleToolDecay, name = 'B')


FinalStateConfig(TupleBu2D0taunuNonPhys.tau_pion)
FinalStateConfig(TupleBu2D0taunuNonPhys.d0_pion)
FinalStateConfig(TupleBu2D0taunuNonPhys.d0_kaon)
#DalitzConfig(TupleBu2D0taunuNonPhys.tau)
LifetimeConfig(TupleBu2D0taunuNonPhys.tau)
ParentConfig(TupleBu2D0taunuNonPhys.tau)
ParentConfig(TupleBu2D0taunuNonPhys.D0)
ParentConfig(TupleBu2D0taunuNonPhys.B)
CommonConfig(TupleBu2D0taunuNonPhys.tau_pion)
CommonConfig(TupleBu2D0taunuNonPhys.d0_pion)
CommonConfig(TupleBu2D0taunuNonPhys.tau)
CommonConfig(TupleBu2D0taunuNonPhys.D0)
CommonConfig(TupleBu2D0taunuNonPhys.B)
CommonConfig(TupleBu2D0taunuNonPhys.d0_kaon)
TISTOSToolConfig(TupleBu2D0taunuNonPhys.B)
TupleToolConfig(TupleBu2D0taunuNonPhys)
TupleToolMCConfig(TupleBu2D0taunuNonPhys)

GaudiSequencer("SeqTupleBu2D0taunuNonPhys").Members.append(TupleBu2D0taunuNonPhys)

SeqTupleBu2D0taunuNonPhys.IgnoreFilterPassed = True
GaudiSequencer("SeqSelBu2D0taunuNonPhys").Members.append(SeqTupleBu2D0taunuNonPhys)

