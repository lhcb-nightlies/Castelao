// Include files

// from Gaudi
#include "GaudiKernel/ToolFactory.h"

// local
#include "TupleToolVtxIsolnPlus.h"
#include "LoKi/IHybridFactory.h"
#include <Kernel/GetIDVAlgorithm.h>
#include <Kernel/IDVAlgorithm.h>
#include <Kernel/IDistanceCalculator.h>
#include <Kernel/IVertexFit.h>
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "Event/Particle.h"

#include "LoKi/AParticles.h"
#include "LoKi/Particles20.h"
using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : TupleToolVtxIsolnPlus
//
// @author Mitesh Patel, Patrick Koppenburg
// @date   2008-04-15
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
// actually acts as a using namespace TupleTool
DECLARE_TOOL_FACTORY( TupleToolVtxIsolnPlus )

	//=============================================================================
	// Standard constructor, initializes variables
	//=============================================================================
TupleToolVtxIsolnPlus::TupleToolVtxIsolnPlus( const std::string& type,
		const std::string& name,
		const IInterface* parent )
	: TupleToolBase ( type, name , parent )
	, m_dva(0)
	, m_dist(0)
		   , m_pVertexFit(0)
{
	declareInterface<IParticleTupleTool>(this);
	m_inputParticles.push_back("/Event/Phys/StdNoPIDsPions");
	//m_inputParticles.push_back("/Event/Phys/StdNoPIDsKaons");
	//m_inputParticles.push_back("/Event/Phys/StdNoPIDsMuons");
	//m_inputParticles.push_back("/Event/Phys/StdNoPIDsElectrons");
	//m_inputParticles.push_back("/Event/Phys/StdNoPIDsProtons");
	declareProperty( "VertexFit", m_typeVertexFit = "default");
	declareProperty("InputParticles", m_inputParticles );
	declareProperty("MaxIPChi2", m_maxIPChi2=25.0);
	declareProperty("MaxTRGHP", m_maxTRGHP=0.5);
}

//=============================================================================

StatusCode TupleToolVtxIsolnPlus::initialize()
{
	const StatusCode sc = TupleToolBase::initialize();
	if ( sc.isFailure() ) return sc;

	m_dva = Gaudi::Utils::getIDVAlgorithm ( contextSvc(), this ) ;
	if (!m_dva) return Error("Couldn't get parent DVAlgorithm",
			StatusCode::FAILURE);

	m_dist = m_dva->distanceCalculator ();
	if ( !m_dist )
	{
		Error("Unable to retrieve the IDistanceCalculator tool");
		return StatusCode::FAILURE;
	}

	m_pVertexFit= m_dva->vertexFitter();
	if ( !m_pVertexFit )
	{
		Error("Unable to retrieve the IVertexFit tool");
		return StatusCode::FAILURE;
	}

	return sc;
}

//=============================================================================

StatusCode TupleToolVtxIsolnPlus::fill( const Particle* mother
		, const Particle* P
		, const std::string& head
		, Tuples::Tuple& tuple )
{

	const std::string prefix = fullName(head);
	const unsigned int maxVtx = 100;
	Assert( P && mother && m_dist
			, "This should not happen, you are inside TupleToolVtxIsolnPlus.cpp :(" );

	bool test = true;
	if(P->isBasicParticle() || isPureNeutralCalo(P) ){
		Error("Not a composite particle! Can't vertex it.");
		return StatusCode::FAILURE;
	}

	// --------------------------------------------------

	const LHCb::Vertex* vtx = P->endVertex();
	if ( msgLevel(MSG::DEBUG) )
	{
		debug() << "vertex for P, ID " << P->particleID().pid() 
			<< " = " << vtx << " at " << vtx->position() << endmsg;
	}
	if ( !vtx )
	{
		Error("Can't retrieve the  vertex for " + prefix );
		return StatusCode::FAILURE;
	}

	const LoKi::Types::Fun m_TRGHP = LoKi::Cuts::TRGHP; //Track ghostprob of particle
	const LoKi::Types::Fun m_TRCHI2DOF = LoKi::Cuts::TRCHI2DOF; //Track chi2/dof of particle
	const LoKi::Types::Fun m_ipchi2 = LoKi::Cuts::IPCHI2(vtx,m_dist); //impact parameter of new tracks WRT old vertex
	const LoKi::Types::Fun m_P = LoKi::Cuts::P;
	const LoKi::Types::Fun m_ETA = LoKi::Cuts::ETA;
	const LoKi::Types::Fun m_PT = LoKi::Cuts::PT; 
	const LoKi::Types::Fun m_PIDK = LoKi::Cuts::PIDK; 
	const LoKi::Types::Fun m_PIDe = LoKi::Cuts::PIDe;
	const LoKi::Types::Fun m_PIDmu = LoKi::Cuts::PIDmu; 
	const LoKi::Types::Fun m_PIDp = LoKi::Cuts::PIDp; 
	const LoKi::Types::Fun m_MM = LoKi::Cuts::MM; 
    //const LoKi::Types::Fun m_BPVDIRA = LoKi::Cuts::BPVDIRA;
	const LoKi::Types::Fun m_BPVDIRA = LoKi::Particles::CosineDirectionAngleWithTheBestPV();
	const LoKi::Cuts::BPVIPCHI2 m_BPVIPCHI2;
	const LoKi::Types::Fun m_ID = LoKi::Cuts::ID;
	const LoKi::Types::Fun m_DOCACHI2MAX = LoKi::Cuts::DOCACHI2MAX;
	const LoKi::Types::Fun m_VCHI2 = LoKi::Cuts::VFASPF(LoKi::Cuts::VCHI2);
	const LoKi::Types::Fun m_VCHI2VDOF = LoKi::Cuts::VFASPF(LoKi::Cuts::VCHI2PDOF);
	const double loki_oldPVCHI2 = m_VCHI2(P);
	const double loki_oldPVCHI2VDOF = m_VCHI2VDOF(P);



	//--------------------------------------------------
	// Get all the particle's final states
	LHCb::Particle::ConstVector source;
	LHCb::Particle::ConstVector target;
	LHCb::Particle::ConstVector finalStates;
	LHCb::Particle::ConstVector parts2Vertex;

	//   const LHCb::Particle* prefix = P;
	source.push_back(P);

	// The first iteration is for the particle to filter, which has an endVertex
	do 
	{
		target.clear();
		for(LHCb::Particle::ConstVector::const_iterator isource = source.begin();
				isource != source.end(); isource++){

			if(!((*isource)->daughters().empty()))
			{

				const LHCb::Particle::ConstVector& tmp = (*isource)->daughtersVector();

				for ( LHCb::Particle::ConstVector::const_iterator itmp = tmp.begin();
						itmp != tmp.end(); ++itmp )
				{
					target.push_back(*itmp);

					// Add the final states, i.e. particles with proto and ignoring gammas
					if((*itmp)->proto() && !isPureNeutralCalo(*itmp) ) finalStates.push_back(*itmp);
				}
			} // if endVertex
		} // isource
		source = target;
	} while(target.size() > 0);

	if (msgLevel(MSG::DEBUG)) debug() << "Final states size= " <<  finalStates.size()  << endreq;


	//--------------------------------------------------
	// Build vector of particles, excluding signal

	LHCb::Particle::ConstVector theParts;



	for(std::vector<std::string>::iterator i = m_inputParticles.begin();
			i !=m_inputParticles.end(); ++i){

		if (!exist<LHCb::Particle::Range>(*i+"/Particles")){
			if (msgLevel(MSG::DEBUG)) debug() << "No particles at " << *i << " !!!!!" << endreq;
			continue;
		}

		LHCb::Particle::Range parts = get<LHCb::Particle::Range>(*i+"/Particles");

		if (msgLevel(MSG::DEBUG)) debug() << "Getting particles from " << *i
			<< " with " << (parts).size() << " particles" << endreq;

		for(LHCb::Particle::Range::const_iterator iparts = (parts).begin();
				iparts != (parts).end(); ++iparts)
		{

			// Ignore if no proto
			if(!(*iparts)->proto()) continue;
			// Ignore if proto and gammas
			if(isPureNeutralCalo(*iparts)) continue;
			// Ignore if particle fails to meet selection criteria: 
			if(m_TRGHP(*iparts) > m_maxTRGHP) continue;
			if(m_ipchi2(*iparts) > m_maxIPChi2) continue;

			// Compare protos, not pointers (e.g. because of clones)
			bool isSignal = false;
			for(LHCb::Particle::ConstVector::const_iterator signal = finalStates.begin();
					signal != finalStates.end(); ++signal){
				const LHCb::ProtoParticle* orig = (*signal)->proto();
				if(!orig) continue;
				if(orig != (*iparts)->proto()) continue;
				isSignal = true;
				break;
			}

			// Ignore if it is a signal particle
			if(isSignal) continue;

			// Check that the same particle does not appear twice
			/*
			bool isIn = false;
			for(LHCb::Particle::ConstVector::const_iterator result = theParts.begin();
					result != theParts.end(); ++result){
				const LHCb::ProtoParticle* orig = (*result)->proto();
				if(!orig) continue;
				if(orig != (*iparts)->proto()) continue;
				isIn = true;
				break;
			}

			// Already in ?
			if(isIn) continue;
			if(!isIn) theParts.push_back(*iparts);
			*/
			theParts.push_back(*iparts);

		} // iparts
	} // m_inputParticles

	if (msgLevel(MSG::DEBUG)) 
		debug() << "Number of particles to check excluding signal, particles with same proto and gammas = "
			<< theParts.size() << endreq;
	//--------------------------------------------------

	//Make vector of current daughter particles
	parts2Vertex = P->daughtersVector();

	if (msgLevel(MSG::DEBUG))
		debug() <<"Now final states should include only your particles direct desendents. finalStates.size()= "
			<<  finalStates.size()  <<endreq;



	std::vector<double> trkTRGHP, trkIPChi2, trkP, trkPT, trkETA, trkPIDe, trkPIDmu, trkPIDK, trkPIDp, trkID, newPBPVDIRA, newPDOCACHI2MAX, newPBPVIPCHI2, newPPT, newPP, newPMM, newPVCHI2, newPVCHI2VDOF, newPETA;
	unsigned int nGood=0;
	// Now count how many particles are compatible with the vertex of the particle under study
	for ( LHCb::Particle::ConstVector::const_iterator iparts = theParts.begin();
			iparts != theParts.end(); ++iparts)
	{


		LHCb::Vertex vtxWithExtraTrack;
		LHCb::Particle newP(*P);

		// Temporarily add the extra track to the parts2Vertex
		parts2Vertex.push_back(*iparts);
		// if (msgLevel(MSG::DEBUG)) debug() << "Adding trk pid"  << (*iparts)->particleID().pid() << " to vtx" << endmsg;

		// Fit

		StatusCode sc = m_pVertexFit->fit (parts2Vertex,vtxWithExtraTrack,newP);
		//StatusCode sc = m_pVertexFit->fit (vtxWithExtraTrack,parts2Vertex);

		// replaced by V.B. 20.Aug.2k+9: (parts2Vertex,vtxWithExtraTrack);
		// Remove the added track from parts2Vertex

		parts2Vertex.pop_back();
		if (!sc)
		{
			// Warning("Failed to fit vertex").ignore();
			// return sc;
		}else{

			if(nGood > maxVtx){
				Warning("Too many compatible track + vertex combinations! Tighten your cuts. Only filling 100 for this event").ignore();
				continue;
			}
			double loki_trkTRGHP = m_TRGHP(*iparts);
			trkTRGHP.push_back(loki_trkTRGHP);
			double loki_trkIPChi2 = m_ipchi2(*iparts); 
			trkIPChi2.push_back(loki_trkIPChi2);
			double loki_trkP = m_P(*iparts); 
			trkP.push_back(loki_trkP);
			double loki_trkETA = m_ETA(*iparts); 
			trkETA.push_back(loki_trkETA);
			double loki_trkPT = m_PT(*iparts);
			trkPT.push_back(loki_trkPT);
			double loki_trkPIDe = m_PIDe(*iparts);
			trkPIDe.push_back(loki_trkPIDe);
			double loki_trkPIDmu = m_PIDmu(*iparts);
			trkPIDmu.push_back(loki_trkPIDmu);
			double loki_trkPIDK = m_PIDK(*iparts);
			trkPIDK.push_back(loki_trkPIDK);
			double loki_trkPIDp = m_PIDp(*iparts);
			trkPIDp.push_back(loki_trkPIDp);
			double loki_trkID = m_ID(*iparts);
			trkID.push_back(loki_trkID);
			double loki_newPBPVDIRA = m_BPVDIRA(&newP);
			newPBPVDIRA.push_back(loki_newPBPVDIRA);
			double loki_newPDOCACHI2MAX = m_DOCACHI2MAX(&newP);
			newPDOCACHI2MAX.push_back(loki_newPDOCACHI2MAX);
			double loki_newPBPVIPCHI2 = m_BPVIPCHI2(&newP); 
			newPBPVIPCHI2.push_back(loki_newPBPVIPCHI2);
			double loki_newPPT = m_PT(&newP);
			newPPT.push_back(loki_newPPT);
			double loki_newPP = m_P(&newP);
			newPP.push_back(loki_newPP);
			double loki_newPETA = m_ETA(&newP);
			newPETA.push_back(loki_newPETA);
			double loki_newPMM = m_MM(&newP);
			newPMM.push_back(loki_newPMM);
			double loki_newPVCHI2VDOF = m_VCHI2VDOF(&newP);
			newPVCHI2VDOF.push_back(loki_newPVCHI2VDOF);
			double loki_newPVCHI2 = m_VCHI2(&newP);
			newPVCHI2.push_back(loki_newPVCHI2);
			nGood++;
		}

	} // iparts

	//FIXME: Fill tuple here


	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_TRGHP", trkTRGHP, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_IPChi2", trkIPChi2, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_ETA", trkETA, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_P", trkP, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_PT", trkPT, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_PIDe", trkPIDe, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_PIDmu", trkPIDmu, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_PIDK", trkPIDK, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_PIDp", trkPIDp, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_trk_LOKI_ID", trkID, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_BPVDIRA", newPBPVDIRA, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_DOCACHI2MAX", newPDOCACHI2MAX, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_VCHI2VDOF", newPVCHI2VDOF, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_VCHI2", newPVCHI2, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_BPVIPCHI2", newPBPVIPCHI2, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_PT", newPPT, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_P", newPP, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_ETA", newPETA, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->farray( prefix+"_isoPlus_newP_LOKI_MM", newPMM, prefix+"_isoPlus_nGood",  maxVtx );
	test &= tuple->column( prefix+"_isoPlus_oldP_LOKI_VCHI2", loki_oldPVCHI2 );
	test &= tuple->column( prefix+"_isoPlus_oldP_LOKI_VCHI2VDOF", loki_oldPVCHI2VDOF );


	return StatusCode(test);
}

