#ifndef MPATEL_TUPLETOOLVTXISOLNPLUS_H
#define MPATEL_TUPLETOOLVTXISOLNPLUS_H 1

// Include files
// from Gaudi
#include "DecayTreeTupleBase/TupleToolBase.h"
#include "Kernel/IParticleTupleTool.h"            // Interface
#include "LoKi/LoKiPhys.h"
#include "LoKi/Child.h"
#include "LoKi/ParticleCuts.h"
#include "LoKi/VertexCuts.h"
#include "LoKi/AParticleCuts.h"
#include "LoKi/AParticles.h"
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/CoreCuts.h"

#include "LoKi/Particles20.h"
#include "LoKi/AParticles.h"
class IDVAlgorithm;
class IDistanceCalculator;
class IVertexFit;

namespace LHCb {
  class Particle;
  class Vertex;
}

/** @class TupleToolVtxIsolnPlus TupleToolVtxIsolnPlus.h
 *
 * \brief Fill isolation information for DecayTreeTuple
 *
 * - head_NOPARTWITHINDCHI2WDW : no. of non-signal particles that when added to vertex give delta chi2 < specified window
 * - head_NOPARTWITHINCHI2WDW : no. of non-signal particles that when added to vertex give chi2 < specified window
 * head_SMALLESTCHI2: chi2 of smallest chi2 combination with any of the input Particles
 * head_SMALLESTDELTACHI2: delta chi2 of smallest delta chi2 combination with any of the input Particles
 *
 * \sa DecayTreeTuple
 *
 *  @todo Maybe one should get Tracks instead of Particles?
 *
 *  @author Mitesh Patel, Patrick Koppenburg
 *  @date   2008-04-15
 */
class TupleToolVtxIsolnPlus : public TupleToolBase,
                          virtual public IParticleTupleTool
{

public:

  /// Standard constructor
  TupleToolVtxIsolnPlus( const std::string& type,
                     const std::string& name,
                     const IInterface* parent);

  virtual ~TupleToolVtxIsolnPlus( ){}; ///< Destructor

  virtual StatusCode initialize();

  StatusCode fill( const LHCb::Particle*
                   , const LHCb::Particle*
                   , const std::string&
                   , Tuples::Tuple& );

private:

  IDVAlgorithm* m_dva;
  const IDistanceCalculator* m_dist;
  const IVertexFit* m_pVertexFit;
  double m_maxIPChi2;
  double m_maxTRGHP;

  std::string m_typeVertexFit;
  std::vector<std::string> m_inputParticles;

};

#endif // MPATEL_TUPLETOOLGEOMETRY_H
