!-----------------------------------------------------------------------------
! Package     : Hlt/HltPython
! Responsible : Diego Martinez Santos 
! Purpose     : Exposing Hlt tools to Python, share common python tools
!-----------------------------------------------------------------------------

! 2016-12-19 - Carlos Vazquez Sierra
  - First release after migration to gitlab. Information about commits on SVN are lost. 
    To be sure, just make version v2r4.

!============================================== v2r3 2014-9-28 ==============================
! 2014-9-28 Diego Martinez Santos
	 Make version v2r3
	 
!============================================== v2r2 2014-3-13 ==============================
! 2014-3-13 Diego Martinez Santos
	 Make version v2r2
! 2014-01-23 - Marco Clemencic
 - Added CMake configuration file.

!============================== v2r0 07-09-2010 ==============================
! 2010-09-07 - Diego Martinez Santos
 - Make version v2r0. Moved all *.py to python/HltPython to avoid conflicts with other packages. They will be zipped in Installarea, and accessible through from HltPython.<whatever> import <what_you_wish>
! 2009-10-21- Antonio Perez-Calero
 - A few more particles added to pidtools.py dictionary
 
!========================== HltPython v1r17 2009-08-10 ===================
! 2009-08-04- Gerhard Raven
 - remove manipulations of PYTHONPATH from the requirements which add
   four directories to it -- please use the 'normal' installation in 
   the InstallArea instead, and fully specify any 'import' statement.
   (done in order to limit the amount of files tried to open during
   startup of the Hlt in the pit)

! 2009-07-24- Antonio Perez-Calero
 - in module descriptors.py change descriptors from strings to lists of strings, so 
 that it can be iterated over in case of a "cocktail sample" like B2hh, where signal may be 
 described by several strings

!========================== HltPython v1r16 2009-07-03 ===================
! 2009-07-01- Antonio Perez-Calero
 - added some more particles to pidtools dictionary

!========================== HltPython v1r15 2009-06-03 ===================
! 2009-05-30- Gerhard Raven
 - do not change GaudiPython location in PYTHONPATH

! 2009-05-13- Antonio Perez-Calero
    - added pointing and dimuon mass functions in kintools.py
    - updated some files in benchmarkchannels.py

!========================== HltPython v1r14 2009-03-23 ===================
! 2009-03-23- Jose A. Hernando
    - PlotTools, hltconf, hltmonitortools adaptations

! 2009-02-25- Sandra Amato
   - hltalleymonitor.py and hltmonitortools.py : Remove HltSummaryTool, substitute by TisTosTool
  
!========================== HltPython v1r13 2009-02-23 ===================
! 2009-02-12- Gerhard Raven
   - remove PrepareMC.opts as it uses $STDOPTS/MCDstContent.opts which will be removed

!========================== HltPython v1r12 2009-01-15 ===================
! 2009-01-13- Jose A. Hernando
   - in python/modules hltconf, hltmonitortools adaptation to the HltSummaryTool and ANNSvc
   - in python/examples hltalleymonitor adaptations to the HltSummaryTool and ANNSvc
   - in python/examples hltexamples adaptions to HltSummaryTool and ANNSvc (to be improved)

! 2008-12-19- Jose A. Hernando
   - in python/modules hltmonitortools decoration changes

! 2008-12-17 - Jose A. Hernando
   - in python/examples hltalleymonitor to monitor un alley
     python hltalleymonitor.py
   - in python/modules hltmonitortools (pyalgorithms) to monitor the alley
   - in python/modules hltconf and adding datacard for lumi2 mb DC06  

!========================== HltPython v1r11 2008-11-24 ===================
Loads of undocumented changes

!========================== HltPython v1r10 2008-10-03 ===================
! 2008-10-20 - Xabier Cid Vidal
   - correction of bugs in ghostsClassifyTools

! 2008-10-15 - Xabier Cid Vidal
   - general update of hadHLTpies,oqTools,freqAndBpercentage,tracksAndVerticesClassifyTools and low level modules: holesIntrudersTools.py, ghostsClassifyTools and causeTools. Adequation to new HLT version (DaVinci v19r14) and reprogramming of some functions.
	- mctools.py change from deprecated gaudimodule to GaudiPython

!========================== HltPython v1r9 2008-08-28 ===================
! 2008-9-17 Antonio Perez-Calero
   - python/modules deleted hltbchannels.py. Changes by Jose A. moved to benchmarkchannels.py. Also, updated datacards to be used. Use this module instead.  

! 2008-8-15 Jose A. Hernando
   - python/examples hltalleymonitor.py updating main alley monitor script
   - python/modules ostools.py tools to deal with the os
   - python/modules datacard_dstL0_ mblumi2_2 datacards for the stripped L0 for hltbchannels.py

!========================== HltPython v1r8 2008-08-06 ===================
! 2008-7-31 - Antonio Perez-Calero and Hugo Ruiz
   - renamed hltbchannels.py module to benchmarkchannels.py and added some more methods taken from descriptors.py
	 - modified descriptors.py. several functions now moved to benchmarkchannels.py
	 - removed old selections files OfflSelBd2DstarMuNu.opts and OfflSelBd2PiK.opts (obsolete now and updated now with DC06 selections by Hans)

! 2008-7-21 - Jose A. Hernando
	 - python/modules improvements in PlotTools, pidtools
   - python/examples hltmonitoralley (almost final script to monitor alleys) 

! 2008-7-17 - Jose A. Hernando
   - python/modules hltconf method to get the hltconfiguration and hltfilters
   - python/modules arguments method to get easily the arguments from command line
   - python/modules hltbchannles dictionaries with dst,opts, etc for the benchmarck channels selected by Hans (prepared by Antonio)
   - python/examples hltalleymonitor.py extending functionality

!========================== HltPython v1r7 2008-07-09 ===================
! 2008-7-9 - Jose A. Hernando
   - hltconf hltalleymonitor.py fixing bugs, be sure they can be documented by doxygen

! 2008-7-2 - Hugo Ruiz
   - dsttools: corrected namespace

! 2008-7-2 - Jose A. Hernando
   - hltalleymonitor.py replaces hlttime.py to do the internal monitoring of the alley

! 2008-7-1 - Gabriel Guerrer
  - replaced PlotTools and PyTree by propper commented and exemplified files. PyTree also had
    some internal changes which made it's use more simple

! 2008-6-27 - Gabriel Guerrer
  - add AlgoTools to modules. Methods for printing Gaudi sequencer hierarchy + algorithm properties

! 2008-6-26 - Jose A. Hernando
  - remove obsolete examples for the hadron alley
  - add hltconf in modules, to get a list of algorithms and properties for a given alley
  - add hlttime in examples, to monitor the rate, and time of an alley

!========================== HltPython v1r6 2008-06-09 ===================
! 2008-6-10 - Xabier Cid
  - update hadHLTPies

! 2008-6-6 - Jose A. Hernando
  - updating python/examples/hltexamples.py
  - adding python/examples/hlttime.py to get the time and rate of an alley

!========================== HltPython v1r5 2008-04-25 ===================
Undocumented changes

!========================== HltPython v1r4 2008-03-07 ===================
! 2008-3-25 - Xabier Cid
  - Style changes in oqTools

! 2008-3-22 - Xabier Cid
  - Add oqTools to modules. Module to deal with 'offline quality' tracks being counterpart to online hadronic HLT ones.
  - Correct frequencyAndBpercentages module and hadHLTpies in examples to have the option to use offline tracks. New functions added also in both to draw output of other functions (previosly everything in same function).
  - Modify behaviour of drawPieLessElements in drawPiesAndHistos module. Now 'Others' category always added in last position.
  - Correct bugs in the files added on 2008-2-17

! 2008-2-21 - Hugo Ruiz for Xabier Cid
  - Corrected bugs in the files added on 2008-2-17

! 2008-2-17 - Hugo Ruiz for Xabier Cid
  - add to modules addDict, ghostClassifyTools, causeTools, 
holesIntrudersTools, frequencyAndBpercentage, 
tracksAndVerticesClassifyTools, drawPiesAndHistos

! 2008-2-15 - Who?
  - remove examples/hltexamples.py
  - in python/examples/hltexamples.py
    bug solved: Vertices->Vertex, and Tracks->Track in candidates()

! 2008-1-29 - Who?
- python/examples/hltexamples.py
  - make the module a script and remove the HLTFAC factory tool
